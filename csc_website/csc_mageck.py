__author__ = 'Alexendar Perez'

######################
#                    #
#    Introduction    #
#                    #
######################

"""

Takes as input read count files from MAGECK and translates them to input for CSC, CSC adjustment is then called
and the specificity adjustment occurs, finally the CSC output is translated back to MAGECK.

"""

###################
#                 #
#    Libraries    #
#                 #
###################

import os
import sys

import numpy as np
import pandas as pd

#from csc import processing
from collections import defaultdict

############################
#                          #
#    Auxillary Function    #
#                          #
############################

def list_object_to_string_concatentation(lst):
    """
    :param lst: list object, list of objects to be converted to string
    :return: string object with values in lst delimited by '_'

    """
    string = ''
    for i in lst:
        string = string + '_' + str(i)

    return string

def combine_list_of_dictionaries_into_single_dictionary_and_return(dict_w_lst_of_dicts):
    """
    """
    empty_dict = {}
    for i in dict_w_lst_of_dicts:
        empty_dict.update(i)

    return empty_dict

def convert_csc_adjusted_logFC_to_adjusted_MAGECK_counts(gRNA_index_count_string_control,
                                                         gRNA_index_count_string_experiment, csc_adjusted_logfc):
    """convert CSC adjusted log2FC to adjusted MAGECK counts
    :param gRNA_index_count_string_control:, string object, string with index positions and unadjusted counts for control group
    :param gRNA_index_count_string_experiment:, string object, string with index positions and unadjusted counts for experimental group
    :param csc_adjusted_logfc: float object, number representing log2FC
    return: string object with index positions and adjusted counts for the experimental group

    """
    control_index_count_lst = gRNA_index_count_string_control.split('+_')[1].split('$_')
    experiment_index_count_lst = gRNA_index_count_string_experiment.split('+_')[1].split('$_')
    control_count_lst = [float(i) for i in control_index_count_lst[1].split('_')]

    experimental_string = ''
    for count in control_count_lst:
        experimental_count = (2 ** csc_adjusted_logfc) * count
        # sys.stdout.write('%s\n' % experimental_count)
        experimental_string = experimental_string + '_' + str(experimental_count)
    experiment_index_count_lst[1] = experimental_string
    novo_gRNA_index_count_string_experiment = 'Index_and_NormalizedCount+_%s$%s' % (
    experiment_index_count_lst[0], experiment_index_count_lst[1])

    return novo_gRNA_index_count_string_experiment

def verbosity(verbose):
    if verbose == 'T':
        sys.stdout.write('full verbosity employed, no cleanup')
    else:
        for f in mageck_to_csc_format_file_lst:
            cmd1 = 'rm %s' % f
            sys.stdout.write('EXECUTE: %s\n' % cmd1)
            os.system(cmd1)

################
#              #
#    Static    #
#              #
################

no_csc_adjustment_writeout_text = 'CSC adjustment not computed for these data given error threshold for model was exceed.' \
                                  'This indicates that the model trained on the data had a RSME above 1.0 which is CSC' \
                                  ' predetermined threshold above which a specificity adjustment is not made.'

no_csc_processing_writeout_text = 'CSC adjustment was unable to be executed. Please ensure that all required files are provided in appropriate format'

#######################
#                     #
#    Core Function    #
#                     #
#######################

#mageck_sample_file = '/Users/perez/Projects/vidigal/crispr_screens/scripts/nci/scripts/csc_website/csc_website_dev/csc_website/mageck_demo_file/novo_sample_mageck_file_for_csc_adjustment.txt'
mageck_sample_file = '/Users/perez/Projects/vidigal/crispr_screens/scripts/nci/scripts/csc_website/csc_website_dev/csc_website/mageck_demo_file/novo_sample_mageck_file_w_avana_gRNAs_for_csc_adjustment.txt'
outdir = '/Users/perez/Desktop'
field_labels = ['SF11411_T0','SF11411_T0','SF11411_T0','SF11411_D28','SF11411_D28','SF11411_D28','NHA_T0','NHA_T0','NHA_T0','NHA_D28','NHA_D28','NHA_D28']
sample_comparisons = ['SF11411_T0:SF11411_D28','NHA_T0:NHA_D28']

sys.stdout.write('MAGECK file read in\n')

# generate comparison dictionary
group_comparison_dict = {}
for compare_group in sample_comparisons:
    groups = compare_group.split(':')
    group_comparison_dict[groups[0]] = groups[1]

# processing of MAGECK count file to CSC adjustment format
gRNA_logFC_aggregate_lst,gRNA_original_counts_dict,sample_count_index_and_value_dict,gRNA_gene_dict = [],defaultdict(list),{},{}
gRNA_original_counts_dict_test = {}
column_names = ['gRNA']
column_names.extend(list(group_comparison_dict.keys()))
with open(mageck_sample_file,'r') as in_file:
    column_header = in_file.readline().strip()
    for line in in_file:
        parts = line.lstrip().rstrip().split()
        gRNA,gene,data_fields = parts[0],parts[1],parts[2:]
        gRNA_gene_dict[gRNA] = gene
        data_fields = [float(i) + 1 for i in data_fields] # add psuedocount
        amount_of_index_slots = len(data_fields)

        # grouping of fields and creation of log2FC
        label_grouping_and_mean_counts_dict = {}
        for s in set(field_labels):
            indices = [index for index,item in enumerate(field_labels) if item == s] # extract index for each UI
            indices_lst = list_object_to_string_concatentation(indices)
            subselection_of_data_fields = [data_fields[i] for i in indices] # extract values in list using index position
            subselection_of_data_fields_lst = list_object_to_string_concatentation(subselection_of_data_fields)
            sample_count_index_and_value_dict[s] = 'Index_and_NormalizedCount+%s$%s' % (indices_lst,subselection_of_data_fields_lst)
            label_grouping_and_mean_counts_dict[s] = np.mean(subselection_of_data_fields)

        for s in set(field_labels):
            gRNA_original_counts_dict[gRNA].append({s:sample_count_index_and_value_dict[s]})
        row_lst = [gRNA]

        for key in group_comparison_dict.keys():
            log2FC = np.log2((label_grouping_and_mean_counts_dict[group_comparison_dict[key]])/(label_grouping_and_mean_counts_dict[key]))
            row_lst.append(log2FC)

        # add row list to aggregate list
        gRNA_logFC_aggregate_lst.append(row_lst)

    # generate dataframe
    df = pd.DataFrame(gRNA_logFC_aggregate_lst,columns=column_names)

# writeouts for data
mageck_to_csc_format_file_lst = []
log2FC_labels,gRNA_label = list(df.columns[1:]),df.columns[0]
for logfc in log2FC_labels:
    df2 = df[[gRNA_label,logfc]]
    df2.to_csv('%s/%s_mageck_file_logfc_conversion_for_csc_adjustment.csv' % (outdir,logfc),index_label=False,index=False)
    mageck_to_csc_format_file_lst.append('%s/%s_mageck_file_logfc_conversion_for_csc_adjustment.csv' % (outdir,logfc))

"""# call CSC adjustment
for f in mageck_to_csc_format_file_lst:
    processing(f,'avana','l',outdir,1.0,20)"""

# aggregate post CSC adjusted files
csc_adjustment_lst,csc_no_adjustment_lst,file_label_lst = [],[],[]
for f in os.listdir(outdir):
    if f.endswith('_csc_adjustment_earth_patched.csv'):
        csc_adjustment_lst.append('%s/%s' % (outdir,f))
        file_label = f.split('csc_output_')[1].split('_mageck_file')[0]
        file_label_lst.append(file_label)
    elif f.endswith('csc_adjustment_CSC_gRNA_Hamming_neighborhood.csv'):
        csc_no_adjustment_lst.append('%s/%s' % (outdir,f))
    else:
        continue

sample_and_csc_adjustment_status_dict = {}
for key in group_comparison_dict.keys():
    if key in file_label_lst:
        sample_and_csc_adjustment_status_dict[key] = 'passed_csc_adjustment_error_threshold'
    else:
        sample_and_csc_adjustment_status_dict[key] = 'failed_csc_adjustment_error_threshold'

# read in CSC adjusted files and correct MAGECK counts
gRNA_original_counts_dict_flat = {}
for key in gRNA_original_counts_dict.keys():
    gRNA_original_counts_dict_flat[key] = combine_list_of_dictionaries_into_single_dictionary_and_return(gRNA_original_counts_dict[key])

gRNA_index_adjusted_count_string_dict = defaultdict(list)
for f in csc_adjustment_lst:
    file_label = f.split('csc_output_')[1].split('_mageck_file')[0]
    if file_label in group_comparison_dict.keys() and sample_and_csc_adjustment_status_dict[file_label] == 'passed_csc_adjustment_error_threshold':
        sys.stdout.write('%s and %s conditions passed CSC adjustment error thresholds, translating corrections into MAGECK file\n' % (file_label,group_comparison_dict[file_label]))
        csc_adjusted_file = pd.read_csv(f)
        gRNA_and_csc_adjusted_logfc = csc_adjusted_file[['gRNA','earth_corrected']]
        gRNA_and_csc_adjusted_logfc_array = np.asarray(gRNA_and_csc_adjusted_logfc)
        for array_row_index in range(len(gRNA_and_csc_adjusted_logfc_array)):
            gRNA,csc_adjusted_logfc = gRNA_and_csc_adjusted_logfc_array[array_row_index][0],gRNA_and_csc_adjusted_logfc_array[array_row_index][1]

            for file_label_key in group_comparison_dict.keys():
                gRNA_index_count_string_control = gRNA_original_counts_dict_flat[gRNA][file_label_key]
                gRNA_index_count_string_experiment = gRNA_original_counts_dict_flat[gRNA][group_comparison_dict[file_label_key]]
                #sys.stdout.write('%s\t%s\n' % (gRNA_index_count_string_control,gRNA_index_count_string_experiment))
                gRNA_index_adjusted_count_string_experiment = convert_csc_adjusted_logFC_to_adjusted_MAGECK_counts(gRNA_index_count_string_control,
                                                                     gRNA_index_count_string_experiment, csc_adjusted_logfc)
                gRNA_index_adjusted_count_string_dict[gRNA].append('adusted_experimental_counts_@_%s%s' % (group_comparison_dict[file_label],gRNA_index_adjusted_count_string_experiment))
                gRNA_index_adjusted_count_string_dict[gRNA].append('non_adusted_control_counts_@_%s%s' % (file_label, gRNA_index_count_string_control))

    else:
        continue

# reconstruct MAGECK input file
filename = mageck_sample_file.split('/')[-1]
for k in sample_and_csc_adjustment_status_dict:
    if sample_and_csc_adjustment_status_dict[k] == 'passed_csc_adjustment_error_threshold':
        with open('%s/csc_adjusted_%s' % (outdir,filename),'w') as outfile:
            column_header_lst = column_header.split('\t')
            for column in column_header_lst:
                outfile.write('%s\t' % column)
            outfile.write('\n')
            for key in gRNA_gene_dict.keys():
                outfile.write('%s\t%s\t' % (key,gRNA_gene_dict[key]))
                lst_with_initial_index_positions_are_zeros = [0]*amount_of_index_slots
                for k in gRNA_index_adjusted_count_string_dict[key]:
                    index_positions,index_values = k.split('+_')[1].split('$_')[0].split('_'),k.split('$_')[1].split('_')

                    # remove psuedocount
                    index_values = [(float(i) -1) for i in index_values]

                    index_value_lst_index = 0
                    for i in index_positions:
                        lst_with_initial_index_positions_are_zeros[int(i)] = index_values[index_value_lst_index]
                        index_value_lst_index += 1

                for corrected_count in lst_with_initial_index_positions_are_zeros:
                    outfile.write('%s\t' % corrected_count)
                outfile.write('\n')

    elif sample_and_csc_adjustment_status_dict[k] == 'failed_csc_adjustment_error_threshold':
        sys.stdout.write('%s and %s conditions failed CSC adjustment error thresholds, no corrections to be made to MAGECK file\n' % (k, group_comparison_dict[k]))
        with open('%s/no_csc_adjusted_explanation_%s_group_within_%s' % (outdir, k, filename), 'w') as outfile:
            outfile.write('%s' % no_csc_adjustment_writeout_text)

    else:
        with open('%s/no_csc_processing_explanation_%s' % (outdir, filename), 'w') as outfile:
            outfile.write('%s' % no_csc_processing_writeout_text)

# clean up
verbosity('T')

#TODO modularize code base
#TODO docstrings on all functions
#TODO make clg

