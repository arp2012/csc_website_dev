################
# SQL Database #
################

###################
#                 #
#    Libraries    #
#                 #
###################
import sys

import boto3
import time

############################
#                          #
#    Auxillary Function    #
#                          #
############################

def query_results(session, params, wait=True):
    client = session.client('athena')

    # This function executes the query and returns the query execution ID
    response_query_execution_id = client.start_query_execution(
        QueryString=params['query'],
        QueryExecutionContext={
            'Database': "default"
        },
        ResultConfiguration={
            'OutputLocation': 's3://' + params['bucket'] + '/' + params['path']
        }
    )

    if not wait:
        return response_query_execution_id['QueryExecutionId']
    else:
        response_get_query_details = client.get_query_execution(
            QueryExecutionId=response_query_execution_id['QueryExecutionId']
        )
        status = 'RUNNING'
        iterations = 360  # 30 mins

        while (iterations > 0):
            iterations = iterations - 1
            response_get_query_details = client.get_query_execution(
                QueryExecutionId=response_query_execution_id['QueryExecutionId']
            )
            status = response_get_query_details['QueryExecution']['Status']['State']

            if (status == 'FAILED') or (status == 'CANCELLED'):
                failure_reason = response_get_query_details['QueryExecution']['Status']['StateChangeReason']
                print(failure_reason)
                return False, False

            elif status == 'SUCCEEDED':
                location = response_get_query_details['QueryExecution']['ResultConfiguration']['OutputLocation']

                # Function to get output results
                response_query_result = client.get_query_results(
                    QueryExecutionId=response_query_execution_id['QueryExecutionId']
                )
                result_data = response_query_result['ResultSet']

                if len(response_query_result['ResultSet']['Rows']) > 1:
                    header = response_query_result['ResultSet']['Rows'][0]
                    rows = response_query_result['ResultSet']['Rows'][1:]

                    header = [obj['VarCharValue'] for obj in header['Data']]
                    result = [dict(zip(header, get_var_char_values(row))) for row in rows]

                    return location, result
                else:
                    return location, None
        else:
            time.sleep(5)

        return False

def get_var_char_values(d):
    return [obj['VarCharValue'] for obj in d['Data']]

def s3_clean_up(bucket_name, s3_tmp_path):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix=s3_tmp_path):
        s3.Object(bucket.name, obj.key).delete()

def convert_sql_query_to_dict(data, gRNA_dict):
    """convert SQL query output to dictionary

    :param data: SQL query output
    :param gRNA_dict: dictionary object, gRNA is key and list object with specificity metrics is values
    :return: dictionary object with gRNA as key and list of gRNA's specificity metrics as values

    """
    for i in range(len(data)):
        gRNA_dict[data[i]['col0']] = [float(data[i]['col1']), int(data[i]['col2']), int(data[i]['col3']),
                                      int(data[i]['col4']), int(data[i]['col5'])]

    return gRNA_dict

def SQL_query(grna_str, region, bucket_name, s3_tmp_path, session):
    """execute SQL query

    :param grna_str: string value, string composed of gRNAs for SQL query
    :param region: string value, AWS region: 'us-west-2'
    :param bucket_name: string value, AWS bucket_name: silico-palmsprings
    :param s3_tmp_path: string value, AWS S3 path: CSC_website_directory/tmp_output/
    :param session: boto3 object, session generated from boto3.Session()
    :return: location and data from SQL query

    """
    sql_query = f"""
               select 
                   *
               from hg38_processed
               where col0 IN ({grna_str})
           """
    # print(sql_query)
    print('guides queried = ' + str(len(grna_str.split(','))))
    params = {
        'region': region,
        'database': 'default',
        'bucket': bucket_name,
        'path': s3_tmp_path,
        'query': sql_query
    }

    # session = boto3.Session()

    # Function for obtaining query results and location
    location, data = query_results(session, params)

    return location, data

def batch_process_gRNAs_for_SQL(gRNA_lst,gRNA_dict,region,bucket_name,s3_tmp_path,session):
    """batch processing of gRNAs for SQL command submission. AWS can only accept 262144 characters per submission

    :param gRNA_lst: list object, list of gRNAs to be processed
    :param gRNA_dict: dictionary object, empty dictionary to be used for gRNAs as keys and list of specificity metrics as values
    :param region: string value, AWS region: 'us-west-2'
    :param bucket_name: string value, AWS bucket_name: silico-palmsprings
    :param s3_tmp_path: string value, AWS S3 path: CSC_website_directory/tmp_output/
    :param session: boto3 object, session generated from boto3.Session()
    :return: dictionary object, gRNA dictionary with gRNAs as keys and list of specificity metrics as values

    """
    grna_str,initial_string,data_clear = '',0,0
    for gRNA in gRNA_lst:
        if (len(grna_str) + len(gRNA)) < 250000: #AWS character limit = 262144
            data_clear = 1
            if initial_string == 0:
                grna_str = repr(gRNA)
                initial_string = 1
            else:
                grna_str = grna_str + ', ' + repr(gRNA)

        else:
            location,data = SQL_query(grna_str,region,bucket_name,s3_tmp_path,session)

            grna_str = ''
            data_clear = 0
            initial_string = 0

            gRNA_dict = convert_sql_query_to_dict(data,gRNA_dict)

            print('guides processed to dict = ' + str(len(gRNA_dict.keys())))


    if data_clear == 1:
        location,data = SQL_query(grna_str,region,bucket_name,s3_tmp_path,session)

        gRNA_dict = convert_sql_query_to_dict(data,gRNA_dict)

        print(len(gRNA_dict.keys()))

    return gRNA_dict


def user_gRNA_upload_to_SQL_database_query_for_gRNA_specificity_metrics_and_dict_creation(post_process_dfname, region,
                                                                                          bucket_name, s3_tmp_path):
    """

    :param post_process_dfname: string value, absolute filepath to user upload file
    :param region: string value, AWS region: 'us-west-2'
    :param bucket_name: string value, AWS bucket_name: silico-palmsprings
    :param s3_tmp_path: string value, AWS S3 path: CSC_website_directory/tmp_output/
    :return: dictionary object, gRNA dictionary with gRNAs as keys and list of specificity metrics as values

    """
    session = boto3.Session()
    gRNA_lst, gRNA_dict = [], {}
    with open(post_process_dfname, 'r') as infile:
        for line in infile:
            parts = line.lstrip().rstrip().split()
            gRNA = '%sNGG' % parts[0]
            gRNA_lst.append(gRNA)

    print('len of list = ', len(gRNA_lst))
    grna_str = ", ".join(repr(e) for e in gRNA_lst)
    print('len of grna_str = ', len(grna_str))  # 262144 characters

    gRNA_dict = batch_process_gRNAs_for_SQL(gRNA_lst, gRNA_dict, region, bucket_name, s3_tmp_path, session)

    return gRNA_dict

#####################
#                   #
#    Development    #
#                   #
#####################

##################
# Xiaoyu version #
##################

"""grna_list = [
    'GAATTAAAAAAAAAAAAAAACNGG',
    'GAATTAAAAAAAAAAAAAAACNGG',
    'CCCAAAAAAAAAAAAAAAACNGG',
    'CCAAAAAAAAAAAAAAAAACNGG',
    'CAAAAAAAAAAAAAAAAAACNGG',
    'TAAAAAAAAAAAAAAAAAACNGG',
    'GGAAAAAAAAAAAAAAAAACNGG',
    'TGAAAAAAAAAAAAAAAAACNGG',
    'GTCAAAAAAAAAAAAAAAACNGG'
]"""

grna_list = [
    'GGAAGTCTGGAGTCTCCAGGNGG','TCAATGGTCACAGTAGCGCTNGG','GTGGACTTCCAGCTACGGCGNGG','GGATGCCCCCAACAAATAATNGG','GGAATCAAATCACAAATCCGNGG']

grna_str = ", ".join(repr(e) for e in grna_list)
print(grna_str)
sql_query = f"""
    select 
        cast(col1 as double) as specificity,
        cast(col2 as int) as h0,
        *
    from hg38_processed
    where col0 IN ({grna_str})
"""
region = 'us-west-2'
bucket_name = 'silico-palmsprings'
s3_tmp_path = 'CSC_website_directory/tmp_output/'
print(sql_query)
params = {
    'region': region,
    'database': 'default',
    'bucket': bucket_name,
    'path': s3_tmp_path,
    'query': sql_query
}

session = boto3.Session()

# Function for obtaining query results and location
location, data = query_results(session, params)
print("Locations: ", location)
print("Result Data: ")
print(data)
# Function for cleaning up the query results to avoid redundant data
s3_clean_up(bucket_name, s3_tmp_path)

#############################
# server script development #
#############################

session = boto3.Session()
region = 'us-west-2'
bucket_name = 'silico-palmsprings'
s3_tmp_path = 'CSC_website_directory/tmp_output/'
post_process_dfname = '/var/folders/w5/4t89w0m561d1jx96w1jq5fzh0000gn/T/tmpjsxbn6nb/Upload.txt'

# TODO make Upload file into gRNA list, perform lookup, create dictionary object #hg38
gRNA_dict = user_gRNA_upload_to_SQL_database_query_for_gRNA_specificity_metrics_and_dict_creation(post_process_dfname,region,bucket_name,s3_tmp_path)

#############################################
# server script with AWS upload development #
#############################################

import boto3
import time
import logging
start = time.time()

def query_results(session, params, wait=True):
    client = session.client('athena')

    # This function executes the query and returns the query execution ID
    response_query_execution_id = client.start_query_execution(
        QueryString=params['query'],
        QueryExecutionContext={
            'Database': "default"
        },
        ResultConfiguration={
            'OutputLocation': 's3://' + params['bucket'] + '/' + params['path']
        }
    )

    if not wait:
        return response_query_execution_id['QueryExecutionId']
    else:
        response_get_query_details = client.get_query_execution(
            QueryExecutionId=response_query_execution_id['QueryExecutionId']
        )
        status = 'RUNNING'
        iterations = 360  # 30 mins

        while (iterations > 0):
            iterations = iterations - 1
            response_get_query_details = client.get_query_execution(
                QueryExecutionId=response_query_execution_id['QueryExecutionId']
            )
            status = response_get_query_details['QueryExecution']['Status']['State']

            if (status == 'FAILED') or (status == 'CANCELLED'):
                failure_reason = response_get_query_details['QueryExecution']['Status']['StateChangeReason']
                print(failure_reason)
                return False, False

            elif status == 'SUCCEEDED':
                location = response_get_query_details['QueryExecution']['ResultConfiguration']['OutputLocation']

                # Function to get output results
                response_query_result = client.get_query_results(
                    QueryExecutionId=response_query_execution_id['QueryExecutionId']
                )
                result_data = response_query_result['ResultSet']

                if len(response_query_result['ResultSet']['Rows']) > 1:
                    header = response_query_result['ResultSet']['Rows'][0]
                    rows = response_query_result['ResultSet']['Rows'][1:]

                    header = [obj['VarCharValue'] for obj in header['Data']]
                    result = [dict(zip(header, get_var_char_values(row))) for row in rows]

                    return location, result
                else:
                    return location, None
        else:
            time.sleep(5)

        return False

def get_var_char_values(d):
    return [obj['VarCharValue'] for obj in d['Data']]

def s3_clean_up(bucket_name, s3_tmp_path):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix=s3_tmp_path):
        s3.Object(bucket.name, obj.key).delete()
    return 'Complete clean up'

def create_session_id():
    from datetime import datetime
    from uuid import uuid4
    eventid = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid4())
    eventid = eventid.replace("-", "_")
    return eventid

def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        import os
        object_name = os.path.basename(file_name)

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except s3_client.exceptions.NoSuchEntityException as e:
        logging.error(e)
        return False
    return object_name

def download_file(s3_object_name, bucket, download_file_path=None):
    """Download a file from S3 bucket to local

    :param s3_object_name: File to upload
    :param bucket: Bucket to upload to
    :param download_file_path: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if download_file_path is None:
        import os
        download_file_path = os.getcwd() + s3_object_name.split('/')[-1]

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.download_file(bucket, s3_object_name, download_file_path)
    except s3_client.exceptions.NoSuchEntityException as e:
        logging.error(e)
        return False
    return 'Success Download'

def query_results_wrapper(sql_str,region,bucket_name,s3_tmp_path,session):
    """Wrap the parameters for execute query to be in a function

    :param sql_str: a string of sql
    :return: True if file was uploaded, else False
    """
    params = {
        'region': region,
        'database': 'default',
        'bucket': bucket_name,
        'path': s3_tmp_path,
        'query': sql_str
    }
    return query_results(session, params)

def convert_SQL_query_upload_process_download_file_to_gRNA_dictionary(temp_result_location):
    """process SQL data from database join and download into gRNA dictionary for further use in CSC website

    :param temp_result_location: string value, absolute filepath to SQL database extracted data file
    :return: dictionary object, gRNA as keys and specificity metrics as values

    """
    gRNA_dict = {}
    with open(temp_result_location, 'r') as infile:
        for line in infile:
            parts = line.lstrip().rstrip().split(',')
            try:
                gRNA, specificity, h0, h1, h2, h3 = parts[0].strip('"'), float(parts[1].strip('"')), int(
                    parts[2].strip('"')), int(parts[3].strip('"')), int(parts[4].strip('"')), int(parts[5].strip('"'))
                gRNA_dict[gRNA] = [specificity, h0, h1, h2, h3]
            except ValueError:
                sys.stderr.write('Value Error: %s, skipping\n' % (parts))

    return gRNA_dict

def upload_user_file_for_SQL_database_query_and_pull_resulting_gRNAs_to_make_gRNA_specificity_dict(input_file,region,bucket_name,s3_tmp_path,s3_input_temp_folder):
    """take user upload file, extract gRNA specificity metrics from S3 database with SQL command, download file, convert to gRNA specificity dictionary

    :param input_file: string input, absolute filepath to user processed input file: Upload.txt
    :param region: string value, AWS region: 'us-west-2'
    :param bucket_name: string value, AWS bucket_name: 'silico-palmsprings'
    :param s3_tmp_path: string value, S3 temporary output directory: 'CSC_website_directory/tmp_output/'
    :param s3_input_temp_folder: string value, S3 temporary input directory: 'CSC_website_directory/tmp_input/'
    :return: gRNA dictionary, keys are gRNA sequences and values as specificity metrics in list format: dict[gRNA] = [specificity,h0,h1,h2,h3]

    """
    # create session
    session = boto3.Session()
    session_id = create_session_id()

    # allow for upload of user data to S3
    s3_upload_unique_path = s3_input_temp_folder + session_id + '/data.tsv'
    upload_file(input_file, bucket_name, s3_upload_unique_path)

    # SQL processing
    table_name = 'tbl_' + session_id
    drop_table_sql = f"""drop table {table_name}"""

    create_table_query = f"""
    CREATE EXTERNAL TABLE {table_name}
        (
            `gRNA` string,
            `lognorm_lnfc` float
        )
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS TEXTFILE
    LOCATION 's3://{bucket_name}/{s3_upload_unique_path}'
    TBLPROPERTIES ('skip.header.line.count'='1');
    """

    alter_sql = f"""
    ALTER TABLE {table_name} SET LOCATION 's3://{bucket_name}/{s3_input_temp_folder}{session_id}'
    """

    # query_results_wrapper(drop_table_sql)
    query_results_wrapper(create_table_query,region,bucket_name,s3_tmp_path,session)
    query_results_wrapper(alter_sql,region,bucket_name,s3_tmp_path,session)

    query_data = f"""
    select 
        *
    from hg38_processed
        where col0 IN (select concat(grna,'NGG') from {table_name})
    """

    # query results
    location, _ = query_results_wrapper(query_data,region,bucket_name,s3_tmp_path,session)

    # download results to local
    s3_output_file = location.replace(f's3://{bucket_name}/', '')
    temp_result_location = f'/Users/perez/Downloads/{session_id}.csv'
    download_file(s3_output_file, bucket_name, temp_result_location)
    query_results_wrapper(drop_table_sql,region,bucket_name,s3_tmp_path,session)
    #s3_clean_up(bucket_name, s3_tmp_path)

    # generate gRNA dictionary for further processing
    gRNA_dict = convert_SQL_query_upload_process_download_file_to_gRNA_dictionary(temp_result_location)

    # time recording
    end = time.time()
    print(end - start)

    return gRNA_dict

region = 'us-west-2'
bucket_name = 'silico-palmsprings'
s3_tmp_path = 'CSC_website_directory/tmp_output/'
input_file = '/var/folders/w5/4t89w0m561d1jx96w1jq5fzh0000gn/T/tmpnt8mhtoj/Upload.txt'
s3_input_temp_folder = 'CSC_website_directory/tmp_input/'

gRNA_dict = upload_user_file_for_SQL_database_query_and_pull_resulting_gRNAs_to_make_gRNA_specificity_dict(input_file,region,bucket_name,s3_tmp_path,s3_input_temp_folder)
