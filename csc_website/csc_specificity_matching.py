__author__ = 'Alexendar Perez'

#####################
#                   #
#   Introduction    #
#                   #
#####################

"""CRISPR Specificity Correction

National Cancer Institute, National Institutes of Health, United States of America
Developer: Alexendar R. Perez M.D., Ph.D
Primary Investigator: Joana A. Vidigal Ph.D
Laboratory: Vidigal Laboratory, 2020

"""

#################
#               #
#   Libraries   #
#               #
#################

import sys
import argparse
import numpy as np
import pickle as pl
import pandas as pd
from math import sqrt

from pyearth import Earth
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

from pkg_resources import resource_exists, resource_filename

# specificity matching #
import matplotlib.pyplot as plt
from scipy.stats import ranksums as ks2

#####################################
#                                   #
#   Specificity Matching Function   #
#                                   #
#####################################

def specificity_matched_safe_harbor_controls(target_gRNAs, safeharbor_negative_control,
                                             percentage_of_library_as_controls, outdir, chunks, std):
    """ create specificity matched controls for a set of gRNAs from the enumeration() script

    :param target_gRNAs: absolute filepath to library gRNAs in the ClassTask format
    :param safeharbor_negative_control: absolute filepath to the safe harbor negative control gRNAs
    :param percentage_of_library_as_controls: float value, value between 0 and 1 that indicates percentage of library to use to make controls, default = 0.10
    :param outdir: string value, absolute filepath to output directory
    :param chunks: int value, the amount of chunks the target_gRNA specificity distribution should be broken into for matching, default = 100
    :param std: float value, the value of the standard deviation around the mean for each chunk on which to sample specificity matched controls, default = 1
    :return: writeout with specificity matched controls

    """
    # specificity pass
    specificity_pass = 1
    attempts = 10

    # filename
    filename = target_gRNAs.split('/')[-1]

    # gRNA read in
    gRNA_df = pd.read_csv(target_gRNAs, sep=',') #names=['gRNA', 'specificity', 'h0', 'h1', 'h2', 'h3'],index_col=False)
    print(gRNA_df)

    # safe harbor read-in
    safe_harbor_df = pd.read_csv(safeharbor_negative_control, sep='\t',
                                 names=['gRNA', 'specificity', 'h0', 'h1', 'h2', 'h3'], index_col=False)
    print(safe_harbor_df)

    # gRNA library size
    library_size = gRNA_df.shape[0]

    # ensuring accurate percentage of controls for library
    if percentage_of_library_as_controls > 1:
        sys.stderr.write(
            'percentage of library as controls is more than 1, allowable values is must be less than 1, defaulting to .01\n')
        percentage_of_library_as_controls = -1
    elif percentage_of_library_as_controls < 0:
        sys.stderr.write(
            'percentage of library as controls is less than 0, allowable values is must be greater than 0 and less than 1, defaulting to .01\n')
        percentage_of_library_as_controls = -1
    else:
        sys.stdout.write('percentage of library as controls is %s\n' % percentage_of_library_as_controls)
        pass

    # size of controls
    if percentage_of_library_as_controls == -1:
        amount_of_safeharbor_gRNA_needed = round(.01 * library_size)
    else:
        amount_of_safeharbor_gRNA_needed = round(percentage_of_library_as_controls * library_size)

    # select N
    N = round(((amount_of_safeharbor_gRNA_needed / float(library_size)) * library_size) / chunks)
    if N == 0:
        N = 1

    # user writeout
    sys.stdout.write('Target gRNA = %s\nSafe Harbor = %s\n' % (target_gRNAs, safeharbor_negative_control))

    # exclude NaN and inf and -inf rows
    gRNA_df = gRNA_df.replace([np.inf, -np.inf, 'inf', '-inf', 'inf ', '-inf '], np.nan).dropna(axis=0)
    safe_harbor_df = safe_harbor_df.replace([np.inf, -np.inf, 'inf', '-inf', 'inf ', '-inf '], np.nan).dropna(axis=0)

    # convert specificity values to numeric
    gRNA_df['specificity'] = pd.to_numeric(gRNA_df['specificity'])
    safe_harbor_df['specificity'] = pd.to_numeric(safe_harbor_df['specificity'])

    # sort based on specificity
    subset_library_gRNA_specificity = sorted(gRNA_df['specificity'])

    # automated selection parameters
    chunks, std, safe_harbor_random_from_auto = automated_specificity_chunking(subset_library_gRNA_specificity, safe_harbor_df, chunks, std, chunks,
                                                 library_size, N, outdir)

    print('auto selected\n')
    print(safe_harbor_random_from_auto)
    # chunk gRNA into specificity quantas
    specificity_chunks = lst_chunking(subset_library_gRNA_specificity, chunks)

    divided_specificity,aggregate_specificity = [],[]
    for i in range(len(specificity_chunks)):
        p_value, count, exhaust_count = 0, 0, 0
        # specificity chunk i
        sc = specificity_chunks[i]
        # get specificity bounds that will be used for matching with safe harbor control
        bound_low, bound_high = np.mean(sc) - (std * np.std(sc)), np.mean(sc) + (std * np.std(sc))
        sys.stdout.write('%s chunk %s\n' % (filename, i))
        # amount of attempts for specificity match
        while count < attempts:
            # select safe harbor gRNAs that are uniquely targeting and that have specificity within the bounds of sc
            safeharbor_df = safe_harbor_df[safe_harbor_df['h0'] == 1]
            safeharbor_df = safeharbor_df[safeharbor_df['specificity'].astype(float) >= bound_low]
            safeharbor_df = safeharbor_df[safeharbor_df['specificity'].astype(float) <= bound_high]
            count += 1
            if len(safeharbor_df) >= N:
                sys.stdout.write('adequate N\n')
                pass
            else:
                sys.stdout.write('inadequate N, total N = %s, represent %s \n' % (len(safeharbor_df),safeharbor_df))
                print(N)
                continue
            try:
                # extract from safe harbor gRNAs, N amount at random from the subsample that is designed to match sc specificity
                safe_harbor_random = safeharbor_df.sample(n=N)

                safe_harbor_random_lst = list(safe_harbor_random['specificity'])
                # compute ks 2 sided statistic to determine if sc and safe harbor list are statistically non-significant
                statistic, p_value_ks = ks2(sc, safe_harbor_random_lst)
                #count += 1
                #exhaust_count += 1
                if p_value_ks >= .05:
                    # if p-value non-significant select for the sample with the highest p-value
                    if p_value_ks > p_value:
                        p_value = p_value_ks
                        tier_select = safe_harbor_random
                        sys.stdout.write('****p-value = %s****\n' % p_value)
                else:
                    exhaust_count += 1
                    if exhaust_count == attempts and p_value < .05:
                        sys.stdout.write('Exhaust Attempts Reached, Uniform Selection of Controls for Specificity Chunk\n')
                        safe_harbor_random = safeharbor_df.sample(n=N)
                        tier_select = safe_harbor_random
                    else:
                        continue

            except ValueError:
                continue

        if p_value == 0:
            safeharbor_df = safe_harbor_df[safe_harbor_df['h0'] == 1]
            safe_harbor_random = safeharbor_df.sample(n=N)
            tier_select = safe_harbor_random

        # list of the safeharbor specificity matched controls. Augments for every new value of sc. Writeout
        divided_specificity.append(tier_select['specificity'].astype(float))
        aggregate_specificity.append(tier_select)
        #tier_select.to_csv('%s/tier_%s_safe_harbor_gRNA_p_val_%s_chunk_%s.csv' % (outdir, filename.split('_')[1], p_value, i),index=False)

    # generate CDF plot that demonstrates specificity matching
    print(aggregate_specificity)
    with open('%s/specificity_matched_controls.csv' % outdir,'a') as outfile:
        for sublist in aggregate_specificity:
            sublist.to_csv(outfile)
            outfile.write('\n')

    flat_lst = [item for sublist in divided_specificity for item in sublist]
    # CDF plot
    sorted_data = np.sort(flat_lst).astype(float)
    # sorted_data = np.sort(np.asarray(tier_select['specificity']).astype(float))
    sorted_data2 = np.sort(np.asarray(subset_library_gRNA_specificity).astype(float))

    statistic, p_value_ks = ks2(sorted_data, sorted_data2)

    yvals = np.arange(len(sorted_data)) / float(len(sorted_data) - 1)
    yvals2 = np.arange(len(sorted_data2)) / float(len(sorted_data2) - 1)

    plt.plot(sorted_data, yvals, label='safe harbor', color='blue')
    plt.plot(sorted_data2, yvals2, label='tier\n pvalue = %s' % (p_value_ks), color='green')

    plt.ylabel('Cumulative Probability')
    plt.xlabel('Specificity')
    plt.title('Tier specificity')
    plt.legend(loc='best')
    filename = filename
    plt.gray()
    plt.grid()
    plt.savefig('%s/cdf_%s.png' % (outdir, filename))
    plt.close()

def lst_chunking(lst,chunk_size):
    """ divide lst object into chunks

    :param lst: lst object to undergo divisions
    :param chunk_size: int value, how many chunks list to be broken into
    :return: divided lst segment

    """
    chunks,lst_of_chunks = round(len(lst)/float(chunk_size)),[]
    for i in range(0,len(lst),chunks):
        chunked_lst = lst[i:i+chunks]
        if len(chunked_lst) == chunks:
            #sys.stdout.write('%s\n' % chunked_lst)
            lst_of_chunks.append(chunked_lst)

    return lst_of_chunks

def automated_specificity_chunking(subset_library_gRNA_specificity, safe_harbor_df, chunks, std, original_chunk,library_size, N, outdir):
    # initial values
    counter = 20
    violations, traverse, count, fail_value = 0, 0, 0, 0
    specificity_pass = 1
    safeharbor_aggregate_lst = []

    # determine chunk the specificity matched data to allow for specificity matching
    specificity_chunks = lst_chunking(subset_library_gRNA_specificity, chunks)

    while specificity_pass == 1:
        for i in range(len(specificity_chunks)):
            sc = specificity_chunks[i]
            bound_low, bound_high = np.mean(sc) - (std * np.std(sc)), np.mean(sc) + (std * np.std(sc))
            safeharbor_df = safe_harbor_df[safe_harbor_df['h0'] == 1]
            safeharbor_df = safeharbor_df[safeharbor_df['specificity'].astype(float) >= bound_low]
            safeharbor_df = safeharbor_df[safeharbor_df['specificity'].astype(float) <= bound_high]
            if len(safeharbor_df) >= N:
                safe_harbor_random = safeharbor_df.sample(n=N)
                safe_harbor_random_lst = list(safe_harbor_random['specificity'])
                statistic, p_value_ks = ks2(sc, safe_harbor_random_lst)
                count += 1
                if p_value_ks >= .05:
                    safeharbor_aggregate_lst.append(safe_harbor_random_lst)
                elif count <= counter:
                    continue
                else:
                    violations += 1
                    sys.stdout.write('statistical violation\n')

            else:
                violations += 1
                sys.stdout.write('sample N violation\n')

        if violations == 0:
            sys.stdout.write('optimal safe harbor chunking parameter = %s, std = %s\n' % (chunks, std))
            break

        else:
            traverse += 1
            chunks = chunks - 1
            if chunks > 3:
                automated_specificity_chunking(subset_library_gRNA_specificity, safe_harbor_df, chunks, std, original_chunk,library_size, N, outdir)
            elif std >= .05:
                chunks = original_chunk
                std -= .05
                automated_specificity_chunking(subset_library_gRNA_specificity, safe_harbor_df, chunks, std, original_chunk,library_size, N, outdir)
            else:
                sys.stdout.write(
                    'Unable to determine specificity match distribution for gRNAs, will assign safeharbor gRNAs in uniform distribution\nchunks = %s\tstd = %s\n' % (chunks, std))
                    #TODO make uniform distribution
                safeharbor_df = safe_harbor_df[safe_harbor_df['h0'] == 1]
                library_size = safeharbor_df.shape[0]
                safe_harbor_random = safeharbor_df.sample(n=.1*library_size)
                safe_harbor_random_lst = list(safe_harbor_random['specificity'])

                fail_value = 1
                break

        specificity_pass = 0

    if fail_value == 1:
        return 1,1,safe_harbor_random_lst
    else:
        return chunks,std,safe_harbor_random_lst

#########################
#                       #
#   Auxillary Function  #
#                       #
#########################

def arg_parser():
    parser = argparse.ArgumentParser()
    hamming_data = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument('-i','--infile',help='absolute filepath to input file',required=True)
    hamming_data.add_argument('-l',dest='library',help='avana, brunello, geckov1, geckov2, tkov, depmap, project_score, sabatini: default is None',default=None)
    hamming_data.add_argument('-g',dest='Hamming',help='absolute filepath to CSC Hamming pickle file: default is None',default=None)
    parser.add_argument('-o','--outdir',help='absolute filepath to output directory',required=True)
    parser.add_argument('--rsm_value',help='float value for residual mean square error threshold for CSC above which no model is applied, default=1.0',default=1.0)
    parser.add_argument('--gRNA_length',help='length of gRNA targeting segment, default=20',default=20)
    parser.add_argument('--specificity_matched_controls',help='allow for specificity matched safe harbor controls for library, default is -1 which is no',default=-1)
    parser.add_argument('--chunks',help='int value, the amount of chunks the target_gRNA specificity distribution should be broken into for matching, default = 20',default=20)
    parser.add_argument('--std',help='float value, the value of the standard deviation around the mean for each chunk on which to sample specificity matched controls, default = 1',default=1)
    parser.add_argument('--percentage_of_library_as_controls',help='float value, value between 0 and 1 that indicates percentage of library to use to make controls, default = 0.10',default=0.10)
    parser.add_argument('--ui_dict',help='dictionary object with gRNA as keys and unique-identifier as values, csc website only',default=None)

    args = parser.parse_args()
    in_file = args.infile
    library = args.library
    genome = args.Hamming
    outdir = args.outdir
    rsm_value = args.rsm_value
    gRNA_length = args.gRNA_length
    specificity_matched_controls = args.specificity_matched_controls
    chunks = args.chunks
    std = args.std
    percentage_of_library_as_controls = args.percentage_of_library_as_controls
    ui_dict = args.ui_dict

    return in_file,library,genome, outdir, float(rsm_value),int(gRNA_length),int(specificity_matched_controls),int(chunks),float(std),float(percentage_of_library_as_controls),ui_dict

def writeout(df, hamming_string_dict, outfile,gRNA_length,ui_dict):
    """write out

    :param df: input dataframe that is made from input file
    :param hamming_string_dict: dictionary object created from hamming string pickle
    :param outfile: opened outfile object
    :return: output file

    """
    count = 0
    df_v = np.asarray(df)
    for i in range(len(df_v)):
        grna = df_v[i][0]
        grna = genomewide_grna_query_format(grna,gRNA_length)
        count += 1
        if count % 1000 == 0:
            sys.stdout.write('%s lines processed\n' % count)
        if grna == 1:
            continue
        for j in df_v[i]:
            outfile.write('%s,' % j)
        try:
            for jj in hamming_string_dict[grna]:
                outfile.write('%s,' % jj)
            specific, h0 = float(hamming_string_dict[grna][0]), float(hamming_string_dict[grna][1])
            if specific >= 0.16 and h0 == 1:
                c = 'above CSC specificity threshold'
            else:
                c = 'below CSC specificity threshold'
            outfile.write('%s,' % c)

        except KeyError:
            sys.stderr.write('\n%s not found in selected library: passing\n' % grna)
            outfile.write('\n%s not present in library\n' % grna)

        try:
            unique_identifier = ui_dict[grna]
            outfile.write('%s\n' % unique_identifier)

        except KeyError:
            sys.stderr.write('\n%s not found in selected library: passing\n' % grna)
            outfile.write('%s_individual_gRNA\n' % grna)


def specificity_metrics(outdir, filename, df, hamming_string_dict,gRNA_length, ui_dict):
    """

    :param outdir: absolute filepath to output directory
    :param filename: name of input file to be used as part of output filename
    :param df: pandas dataframe with first column as gRNA
    :param hamming_string_dict: CSC onboard dictionary object with key as gRNA and value as Hamming metrics
    :return: file with gRNA and specificity metrics

    """
    with open('%s/%s_CSC_gRNA_Hamming_neighborhood.csv' % (outdir, filename), 'w') as outfile:
        outfile.write('%s,%s,%s,%s,%s,%s,%s,%s\n' % (
            'gRNA', 'specificity', 'h0', 'h1', 'h2', 'h3', 'classification','unique_identifier'))
        writeout(df, hamming_string_dict, outfile,gRNA_length,ui_dict)
    sys.stdout.write('write out complete\n%s/%s_CSC_gRNA_Hamming_neighborhood.csv\n' % (outdir, filename))

def csc(df, hamming_string_dict, outdir, filename, rsm_value,gRNA_length):
    """CRISPR Specificity Correction

    :param df: pandas dataframe with first column as gRNA and second column as logFC/metric
    :param hamming_string_dict: CSC onboard dictionary object with key as gRNA and value as Hamming metrics
    :param outdir: absolute filepath to output directory
    :param filename: name of input file to be used as part of output filename
    :return: CSC adjustment

    """
    # MARS compatible file

    #remove NaN and Inf
    df = df.replace([np.inf, -np.inf], np.nan).dropna(axis=0)

    count = 0
    df_mars_lst = []
    df_v = np.asarray(df)
    for i in range(len(df_v)):
        row_lst = []
        grna, metric = df_v[i][0], df_v[i][1]

        if metric:
            pass
        else:
            sys.stdout.write('gRNA %s has metric %s not in compatible format, skipping\n' % (grna,metric))
            continue

        if grna:
            pass
        else:
            sys.stdout.write('gRNA %s not in compatible format, skipping\n' % (grna))
            continue

        grna = genomewide_grna_query_format(grna,gRNA_length)
        count += 1
        if count % 1000 == 0:
            sys.stdout.write('%s lines processed\n' % count)
        if grna == 1:
            continue
        try:
            metric = float(metric)
        except ValueError:
            sys.stdout.write('WARNING: encountered %s which is not float compatible, skipping\n' % metric)
            continue
        row_lst.append(grna)
        try:
            for jj in hamming_string_dict[grna]:
                row_lst.append(jj)
            row_lst.append(metric)
            df_mars_lst.append(row_lst)
        except KeyError:
            sys.stdout.write('\n%s not found in selected library: passing\n' % grna)
            continue

    df = pd.DataFrame(df_mars_lst, columns=['gRNA', 'specificity', 'h0', 'h1', 'h2', 'h3', 'original_value']) #TODO specificity match

    # exclude infinte specificity non-target gRNAs
    df = df[df['h0'] != 0]

    # isolate pertinent confounder variables
    df_confounders = df[['specificity', 'h0', 'h1', 'h2', 'h3']]

    # knots
    knots = df['original_value'].quantile([0.25, 0.5, 0.75, 1])

    # training and testing data

    train_x, test_x, train_y, test_y = train_test_split(df_confounders, df['original_value'], test_size=0.10,
                                                        random_state=1)
    # Fit an Earth model
    model = Earth(feature_importance_type='gcv')
    try:
        model.fit(train_x, train_y)
    except ValueError:
        sys.stdout.write('\nValue Error encountered. Model unable to be trained. Exiting CSC Novo\n%s\n' % model.fit(train_x, train_y))
        model_processed = 'F'
        sys.stdout.write('training input x data\n %s\ntraining input y data\n %s\n' % (train_x,train_y))
        return model_processed

    # Print the model
    print(model.trace())
    print(model.summary())
    print(model.summary_feature_importances())

    # Plot the model
    y_hat = model.predict(test_x)

    # calculating RMSE values
    rms1 = sqrt(mean_squared_error(test_y, y_hat))
    print('\n\nRMSE on Predictions\n\n')
    print(rms1)

    # calculating R^2 for training
    print('\n\nR^2 on Training Data\n\n')
    print(model.score(train_x, train_y))

    # calculating R^2 for testing
    print('\n\nR^2 on Testing Data\n\n')
    print(model.score(test_x, test_y))

    # write out model metrics
    with open('%s/csc_model_metrics_%s.txt' % (outdir, filename), 'w') as outfile:
        outfile.write('%s\n%s\n%s\nRMSE on Predictions\n%s' % (
            model.trace(), model.summary(), model.summary_feature_importances(), rms1))

    if rms1 <= rsm_value:

        #model processed
        model_processed = 'T'

        # full data prediction
        df['earth_adjustment'] = model.predict(df_confounders)

        # CSC correction
        df['earth_corrected'] = df['original_value'] - df['earth_adjustment']

        # main write out
        df.to_csv('%s/csc_output_%s_earth_patched.csv' % (outdir, filename))

        # pickle write out # comment out for website
        model_file = open('%s/csc_output_%s_earth_model.pl' % (outdir, filename), 'wb')
        pl.dump(model, model_file)
        model_file.close()

        sys.stdout.write('\nCSC adjustment complete\n')
        sys.stdout.write('\nCSC output files written to %s\n' % outdir)
        return model_processed

    else:
        sys.stdout.write('\nCSC adjustment not computed as model residual mean squared error exceeds 1.0\n')
        model_processed = 'F'
        return model_processed

def read_in(in_file):
    """multiple attempt read in for generic file

    :param in_file: absolute filepath to input file
    :return: opened file, classification of opening method

    """
    classification = '.csv'
    if '\t' in open(in_file).readline():
        classification = '.txt'

    try:
        infile = pd.read_excel(in_file)
        sys.stdout.write('file read in as Excel\n')

    except:

        try:
            if classification == '.csv':
                infile = pd.read_csv(in_file)
                sys.stdout.write('file read in as csv\n')
            else:
                infile = pd.read_csv(in_file, sep='\t')
                sys.stdout.write('file read in as txt\n')

        except:
            infile = pd.DataFrame(open(in_file, 'r'))
            sys.stdout.write('file read in with python open function and cast as pandas DataFrame\n')

    return infile

def csc_processing(in_file, hamming_string_dict,outdir,rsm_value,gRNA_length,specificity_matched_controls,percentage_of_library_as_controls,chunks,std,safeharbor_negative_control,ui_dict):
    """control function that assessed if CSC adjustment/model deployed or if specificity metrics only are given

    :param in_file: absolute filepath to input file
    :param hamming_string_dict: dictionary object with gRNA as key and hamming string as value
    :return: CSC adjustment or specificity metric output

    """
    # read in file
    df = read_in(in_file)
    filename = in_file.split('/')[-1].split('.')[0]
    columns, rows = len(df.columns), df.shape[0]

    # ensure columns named correctly
    if columns > 1:
        sys.stdout.write(
            '\n%s columns detected\nfirst two columns will be used\n---column one = gRNA---\n---column two = value---\n' % columns)
        df = df.iloc[:, 0:2]
        df.columns = ['gRNA', 'original_value']

        model_processed = csc(df, hamming_string_dict, outdir, filename,rsm_value,gRNA_length)
        if model_processed == 'T':
            pass
        else:
            specificity_metrics(outdir, filename, df, hamming_string_dict, gRNA_length, ui_dict)

    elif columns == 1:
        sys.stdout.write('\nfile determined to have only one column\n---column one = gRNA---\n')
        specificity_metrics(outdir, filename, df, hamming_string_dict, gRNA_length, ui_dict)

        #TODO ensure file exists
        if specificity_matched_controls != -1:
            sys.stdout.write('selecting specificity matched controls\n')
            target_gRNAs = '%s/%s_CSC_gRNA_Hamming_neighborhood.csv' % (outdir, filename)
            specificity_matched_safe_harbor_controls(target_gRNAs, safeharbor_negative_control,
                                                     percentage_of_library_as_controls, outdir, chunks, std)

    else:
        sys.stdout.write('\nfile determined to have no columns. Unable to process\n')
        sys.exit(1)

def load_pickle(f):
    """load pickle file and generate dictionary

    :param f: absolute filepath to CSC library pickle files
    :return: dictionary object (Pandas)

    """
    with open(f, 'rb') as infile:
        pickle_dataframe = pl.load(infile,encoding='latin1')

        try:
            pickle_dictionary = pickle_dataframe.set_index('gRNA').to_dict()
            return pickle_dictionary

        except AttributeError:
            if type(pickle_dataframe) == dict:
                sys.stdout.write('\n%s is a dictionary object\n' % f)
                pickle_dictionary = pickle_dataframe
                return pickle_dictionary

            else:
                sys.stderr.write('\n%s is incompatible pickle file\nHave pickle file be dictionary with gRNA as key and specificity string as value\n' % f)
                sys.exit(1)

def file_load(infile):
    """input parameter selections

    :param infile: name of screen

    :return: filepath for Hamming and correction factor pickles for library

    """
    if infile == 'avana':
        infile_h = 'screen_models/Hamming/avana_patched_Hamming_string.pl'
        h = resource_filename(__name__, infile_h)
        return h

    if infile == 'depmap':
        infile_h = 'screen_models/Hamming/avana_patched_Hamming_string.pl'
        h = resource_filename(__name__, infile_h)
        return h

    if infile == 'project_score':
        infile_h = 'screen_models/Hamming/project_score_patch_format_screen_Hamming_string.pl'
        h = resource_filename(__name__, infile_h)
        return h

    if infile == 'sabatini':
        infile_h = 'screen_models/Hamming/sabatini_patch_format_screen_Hamming_string.pl'
        h = resource_filename(__name__, infile_h)
        return h

    elif infile == 'brunello':
        infile_h = 'screen_models/Hamming/brunello_patch_format_screen_Hamming_string.pl'
        h = resource_filename(__name__, infile_h)
        return h

    elif infile == 'geckov1':
        infile_h = 'screen_models/Hamming/geckov1_patch_format_screen_Hamming_string.pl'
        h = resource_filename(__name__, infile_h)
        return h

    elif infile == 'geckov2':
        infile_h = 'screen_models/Hamming/geckov2_patch_format_screen_Hamming_string.pl'
        h = resource_filename(__name__, infile_h)
        return h

    elif infile == 'example_grna_logfc':
        infile_h = 'screen_models/examples/avana_patched_sample_gRNA_lognorm_lnfc.csv'
        h = resource_filename(__name__, infile_h)
        return h

    elif infile == 'example_grna':
        infile_h = 'screen_models/examples/avana_patched_sample_gRNA.csv'
        h = resource_filename(__name__, infile_h)
        return h

    else:
        sys.stderr.write('%s not a recognized screen\n' % infile)

def processing(in_file,screen,classification,outdir,rsm_value,gRNA_length,specificity_matched_controls,chunks,std,percentage_of_library_as_controls,ui_dict):
    """core processing function

    :param in_file: absolute filepath to input file
    :param screen: string value corresponding to screen name
    :param classification: deploy lite or novo
    :return:
    """

    # supported screens
    screen = screen.lower()
    support_screens = ['avana', 'brunello', 'geckov1', 'geckov2','depmap']

    if classification == 'l':
        # ensure strings all lowercase
        sys.stdout.write('\nCSC Lite deployed\n')
    elif classification == 'g':
        sys.stdout.write('\nCSC Novo deployed\n')

    #convert to 19mer if project score selected
    if screen == 'project_score':
        gRNA_length = 19

    # check if support screen queried
    if screen in support_screens:
        organism_genome = 'hg38' #TODO delete
        sys.stdout.write('loading %s library data\n' % screen)
        h = file_load(screen)

        # load pickle and generate dictionaries
        hamming_dict = load_pickle(h)

        # translate hamming string
        sys.stdout.write('string translation\n')
        hamming_string_dict = {}
        for key in hamming_dict['Hamming_string'].keys():
            float_casted = [float(i) for i in hamming_dict['Hamming_string'][key].split('_')]
            hamming_string_dict[key] = float_casted

        safeharbor_negative_control = '/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/hg38_gRNA_lookup_new_delimiter_human_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'
        csc_processing(in_file, hamming_string_dict, outdir, rsm_value,gRNA_length,specificity_matched_controls,percentage_of_library_as_controls,chunks,std,safeharbor_negative_control,ui_dict)

    elif screen == 'example':
        organism_genome = 'hg38' #TODO delete
        if in_file == 'example_grna_logfc':
            in_file = file_load('example_grna_logfc')
        elif in_file == 'example_grna':
            in_file = file_load('example_grna')
        else:
            sys.stderr.write('ENTER\n"csc_process -i example_grna_logfc -l example"\nOR\n"csc_process -i example_grna -l example"\n')
            sys.exit(1)

        sys.stdout.write('Example\n')
        h = file_load('avana')

        # load pickle and generate dictionaries
        hamming_dict = load_pickle(h)

        # translate hamming string
        sys.stdout.write('string translation\n')
        hamming_string_dict = {}
        for key in hamming_dict['Hamming_string'].keys():
            hamming_string_dict[key] = hamming_dict['Hamming_string'][key].split('_')

        safeharbor_negative_control = '/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/hg38_gRNA_lookup_new_delimiter_human_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'
        csc_processing(in_file, hamming_string_dict, outdir, rsm_value,gRNA_length,specificity_matched_controls,percentage_of_library_as_controls,chunks,std,safeharbor_negative_control,ui_dict)

    else:

        if screen == 'project_score':
            organism_genome = 'hg38'
            sys.stdout.write('Project Score library\n')
            h = file_load('project_score')

            # load pickle and generate dictionaries
            hamming_dict = load_pickle(h)
            gRNA_length = 19

        elif screen == 'sabatini':
            organism_genome = 'hg38'
            sys.stdout.write('Sabatini library\n')
            h = file_load('sabatini')

            # load pickle and generate dictionaries
            hamming_dict = load_pickle(h)

        else:
            sys.stdout.write('\nscreen selection of %s is novel; will attempt load into memory\n' % screen)

            if screen == 'hg38':
                organism_genome = 'hg38'
            elif screen == 'mm10':
                organism_genome = 'mm10'
            else:
                sys.stdout.write('organism genome %s not supported, specificity matched controls not possible\n')
                specificity_matched_controls = -1
                organism_genome = 'not_supported'

            # load pickle and generate dictionaries
            hamming_dict = load_pickle(screen)

        # translate hamming string
        sys.stdout.write('string translation\n')
        hamming_string_dict = {}
        try:
            for key in hamming_dict['Hamming_string'].keys():
                float_casted = [float(i) for i in hamming_dict['Hamming_string'][key].split('_')]
                hamming_string_dict[key] = float_casted
        except KeyError:
            for key in hamming_dict.keys():
                float_casted = [float(i) for i in hamming_dict[key].split('_')]
                hamming_string_dict[key] = float_casted

        if organism_genome == 'hg38':
            safeharbor_negative_control = '/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/hg38_gRNA_lookup_new_delimiter_human_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'
        elif organism_genome == 'mm10':
            safeharbor_negative_control = '/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/mm10_gRNA_lookup_new_delimiter_mouse_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'
        else:
            safeharbor_negative_control = 'not_supported'

        csc_processing(in_file, hamming_string_dict, outdir, rsm_value,gRNA_length,specificity_matched_controls,percentage_of_library_as_controls,chunks,std,safeharbor_negative_control,ui_dict)

def genomewide_grna_query_format(i,gRNA_length):
    """adjust gRNA so that it is able to be queried in genomewide hash tables

    :param i: string value, gRNA
    :return: string value, gRNA with NGG or if not of sufficent gRNA length, exit value 1

    """
    if len(i) == gRNA_length:
        gRNA = '%sNGG' % i
        return gRNA
    elif len(i) > gRNA_length:
        gRNA = i[0:gRNA_length]
        gRNA = '%sNGG' % gRNA
        return gRNA
    else:
        sys.stderr.write('gRNA %s not of length %s: skipping\n' % (i, gRNA_length))
        return 1

#####################
#                   #
#   Main Function   #
#                   #
#####################

def main():

    # user inputs
    in_file,library,genome, outdir, rsm_value,gRNA_length,specificity_matched_controls,chunks,std,percentage_of_library_as_controls,ui_dict = arg_parser()

    if library:
        screen = library
        classification = 'l'
    else:
        screen = genome
        classification = 'g'

    # processing
    processing(in_file,screen,classification, outdir, rsm_value,gRNA_length,specificity_matched_controls,chunks,std,percentage_of_library_as_controls,ui_dict)

    # user end message
    sys.stdout.write('\nprocessing complete\n')

if __name__ == '__main__':
    main()

