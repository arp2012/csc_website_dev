#################
#               #
#   Libraries   #
#               #
#################

import string
import cherrypy

from jinja2 import Environment, FileSystemLoader
import csv
import os
import sys
import os.path
from os import path
import gc
import os.path
import logging
import pickle as pl

current_dir = os.path.dirname(os.path.abspath(__file__))
import cgitb; cgitb.enable()
from io import StringIO
import tempfile
#from csc import processing, csc_processing
from csc_specificity_matching import processing, csc_processing
from gRNA_lookup_from_coordinate_csc_website_version import csc_gRNA_lookup_processing
from cherrypy.lib.static import serve_file
from cherrypy.lib.static import staticfile
from cherrypy.lib.static import serve_download
#from flask import Flask, render_template, url_for, request
#app = Flask(__name__)

env = Environment(loader=FileSystemLoader('templates'), cache_size=0)

# SQL
import boto3
import time

#########################
#                       #
#   Auxillary Function  #
#                       #
#########################

def secureheaders():
    headers = cherrypy.response.headers
    headers['X-Frame-Options'] = 'DENY'
    headers['X-XSS-Protection'] = '1; mode=block'
    headers['Content-Security-Policy'] = "default-src='self'"

def noBodyProcess():
    """direct control of file upload destination"""
    cherrypy.request.process_request_body = False

def expose_as_csv(f):
    """
    awesome trick to expose data as csv

    taken from:
    http://code.activestate.com/recipes/573461-publish-list-data-as-csv-file/
    """

    @cherrypy.expose()
    #@strongly_expire
    def wrap(*args, **kw):
        rows = f(*args, **kw)
        out = StringIO.StringIO()
        writer = csv.writer(out) 
        writer.writerows(rows)
        cherrypy.response.headers["Content-Type"] = "application/csv" #invokes download
        cherrypy.response.headers["Content-Length"] = out.len
        return out.getvalue()
    return wrap

def load_pickle(f):
    """load pickle file and generate dictionary

    :param f: absolute filepath to CSC library pickle files
    :return: dictionary object (Pandas)

    """
    with open(f, 'rb') as infile:
        pickle_dataframe = pl.load(infile,encoding='latin1')

        try:
            pickle_dictionary = pickle_dataframe.set_index('gRNA').to_dict()
            return pickle_dictionary

        except AttributeError:
            if type(pickle_dataframe) == dict:
                sys.stdout.write('\n%s is a dictionary object\n' % f)
                pickle_dictionary = pickle_dataframe
                return pickle_dictionary

            else:
                sys.stderr.write('\n%s is incompatible pickle file\nHave pickle file be dictionary with gRNA as key and specificity string as value\n' % f)
                sys.exit(1)

def feature_dictionary_generation(feature_lst):
    """generate feature dictionary from list of annotation files

    :param feature_lst: list object, has absolute filepath to annotation files
    :return: dictionary object with feature as keys and coordinates as values

    """
    feature_dict = {}
    for annotation in feature_lst:
        with open(annotation, 'r') as infile:
            for line in infile:
                parts = line.lstrip().rstrip().split()
                chromosome, start, stop, feature_name = parts[0], parts[1], parts[2], parts[3]
                feature_dict[feature_name] = '%s:%s-%s' % (chromosome, start, stop)

    return feature_dict

def translate_hamming_string(string, hamming_dict):
    """translate hamming string pickle data for readout by CSC

    :param string: string value, name of organism being processed
    :param hamming_dict: hamming pickle
    :return: dictionary object, key is gRNA and value is specificity metrics

    """
    sys.stdout.write('string translation %s\n' % string)
    hamming_string_dict = {}
    try:
        for key in hamming_dict.keys():
            hamming_string_dict[key] = hamming_dict[key].split('_')
    except AttributeError:
        sys.stderr.write('Attribute Error with %s, re-attempt with different super-key\n' % hamming_dict.keys)
        for key in hamming_dict['Hamming_string'].keys():
            float_casted = [float(i) for i in hamming_dict['Hamming_string'][key].split('_')]
            hamming_string_dict[key] = float_casted

    return hamming_string_dict

def pickle_readin_w_string_translation(f):
    """read in pickle file and conduct string translation

    :param f: string input, absolute filepath to pickle hamming dictionary
    :return: string translated dictionary object

    """
    sys.stdout.write('pickle deserialization and string translation %s\n' % f)
    with open(f, 'rb') as infile:
        pickle_dataframe = pl.load(infile, encoding='latin1')

        try:
            pickle_dictionary = pickle_dataframe
            for k in pickle_dictionary.keys():
                float_casted = [float(i) for i in pickle_dictionary[k].split('_')]
                pickle_dictionary[k] = float_casted
                hamming_string_dict = pickle_dictionary
        except AttributeError:
            pickle_dictionary = pickle_dataframe.set_index('gRNA').to_dict()
            for k in pickle_dictionary['Hamming_string'].keys():
                float_casted = [float(i) for i in pickle_dictionary['Hamming_string'][k].split('_')]
                pickle_dictionary['Hamming_string'][k] = float_casted
                hamming_string_dict = pickle_dictionary['Hamming_string']

    return hamming_string_dict

#########################
#                       #
#   AWS SQL Function    #
#                       #
#########################

def query_results(session, params, wait=True):
    client = session.client('athena')

    # This function executes the query and returns the query execution ID
    response_query_execution_id = client.start_query_execution(
        QueryString=params['query'],
        QueryExecutionContext={
            'Database': "default"
        },
        ResultConfiguration={
            'OutputLocation': 's3://' + params['bucket'] + '/' + params['path']
        }
    )

    if not wait:
        return response_query_execution_id['QueryExecutionId']
    else:
        response_get_query_details = client.get_query_execution(
            QueryExecutionId=response_query_execution_id['QueryExecutionId']
        )
        status = 'RUNNING'
        iterations = 360  # 30 mins

        while (iterations > 0):
            iterations = iterations - 1
            response_get_query_details = client.get_query_execution(
                QueryExecutionId=response_query_execution_id['QueryExecutionId']
            )
            status = response_get_query_details['QueryExecution']['Status']['State']

            if (status == 'FAILED') or (status == 'CANCELLED'):
                failure_reason = response_get_query_details['QueryExecution']['Status']['StateChangeReason']
                print(failure_reason)
                return False, False

            elif status == 'SUCCEEDED':
                location = response_get_query_details['QueryExecution']['ResultConfiguration']['OutputLocation']

                # Function to get output results
                response_query_result = client.get_query_results(
                    QueryExecutionId=response_query_execution_id['QueryExecutionId']
                )
                result_data = response_query_result['ResultSet']

                if len(response_query_result['ResultSet']['Rows']) > 1:
                    header = response_query_result['ResultSet']['Rows'][0]
                    rows = response_query_result['ResultSet']['Rows'][1:]

                    header = [obj['VarCharValue'] for obj in header['Data']]
                    result = [dict(zip(header, get_var_char_values(row))) for row in rows]

                    return location, result
                else:
                    return location, None
        else:
            time.sleep(5)

        return False

def get_var_char_values(d):
    return [obj['VarCharValue'] for obj in d['Data']]

def s3_clean_up(bucket_name, s3_tmp_path):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix=s3_tmp_path):
        s3.Object(bucket.name, obj.key).delete()
    return 'Complete clean up'

def create_session_id():
    from datetime import datetime
    from uuid import uuid4
    eventid = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid4())
    eventid = eventid.replace("-", "_")
    return eventid

def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        import os
        object_name = os.path.basename(file_name)

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except s3_client.exceptions.NoSuchEntityException as e:
        logging.error(e)
        return False
    return object_name

def download_file(s3_object_name, bucket, download_file_path=None):
    """Download a file from S3 bucket to local

    :param s3_object_name: File to upload
    :param bucket: Bucket to upload to
    :param download_file_path: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if download_file_path is None:
        import os
        download_file_path = os.getcwd() + s3_object_name.split('/')[-1]

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.download_file(bucket, s3_object_name, download_file_path)
    except s3_client.exceptions.NoSuchEntityException as e:
        logging.error(e)
        return False
    return 'Success Download'

def query_results_wrapper(sql_str,region,bucket_name,s3_tmp_path,session):
    """Wrap the parameters for execute query to be in a function

    :param sql_str: a string of sql
    :return: True if file was uploaded, else False
    """
    params = {
        'region': region,
        'database': 'default',
        'bucket': bucket_name,
        'path': s3_tmp_path,
        'query': sql_str
    }
    return query_results(session, params)

def convert_SQL_query_upload_process_download_file_to_gRNA_dictionary(temp_result_location):
    """process SQL data from database join and download into gRNA dictionary for further use in CSC website

    :param temp_result_location: string value, absolute filepath to SQL database extracted data file
    :return: dictionary object, gRNA as keys and specificity metrics as values

    """
    gRNA_dict = {}
    with open(temp_result_location, 'r') as infile:
        for line in infile:
            parts = line.lstrip().rstrip().split(',')
            try:
                gRNA, specificity, h0, h1, h2, h3 = parts[0].strip('"'), float(parts[1].strip('"')), int(
                    parts[2].strip('"')), int(parts[3].strip('"')), int(parts[4].strip('"')), int(parts[5].strip('"'))
                gRNA_dict[gRNA] = [specificity, h0, h1, h2, h3]
            except ValueError:
                sys.stderr.write('Value Error: %s, skipping\n' % (parts))

    return gRNA_dict

def upload_user_file_for_SQL_database_query_and_pull_resulting_gRNAs_to_make_gRNA_specificity_dict(input_file,region,bucket_name,s3_tmp_path,s3_input_temp_folder):
    """take user upload file, extract gRNA specificity metrics from S3 database with SQL command, download file, convert to gRNA specificity dictionary

    :param input_file: string input, absolute filepath to user processed input file: Upload.txt
    :param region: string value, AWS region: 'us-west-2'
    :param bucket_name: string value, AWS bucket_name: 'silico-palmsprings'
    :param s3_tmp_path: string value, S3 temporary output directory: 'CSC_website_directory/tmp_output/'
    :param s3_input_temp_folder: string value, S3 temporary input directory: 'CSC_website_directory/tmp_input/'
    :return: gRNA dictionary, keys are gRNA sequences and values as specificity metrics in list format: dict[gRNA] = [specificity,h0,h1,h2,h3]

    """
    # create session
    session = boto3.Session()
    session_id = create_session_id()

    # allow for upload of user data to S3
    s3_upload_unique_path = s3_input_temp_folder + session_id + '/data.tsv'
    upload_file(input_file, bucket_name, s3_upload_unique_path)

    # SQL processing
    table_name = 'tbl_' + session_id
    drop_table_sql = f"""drop table {table_name}"""

    create_table_query = f"""
    CREATE EXTERNAL TABLE {table_name}
        (
            `gRNA` string,
            `lognorm_lnfc` float
        )
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS TEXTFILE
    LOCATION 's3://{bucket_name}/{s3_upload_unique_path}'
    TBLPROPERTIES ('skip.header.line.count'='1');
    """

    alter_sql = f"""
    ALTER TABLE {table_name} SET LOCATION 's3://{bucket_name}/{s3_input_temp_folder}{session_id}'
    """

    # query_results_wrapper(drop_table_sql)
    query_results_wrapper(create_table_query,region,bucket_name,s3_tmp_path,session)
    query_results_wrapper(alter_sql,region,bucket_name,s3_tmp_path,session)

    query_data = f"""
    select 
        *
    from hg38_processed
        where col0 IN (select concat(grna,'NGG') from {table_name})
    """

    # query results
    location, _ = query_results_wrapper(query_data,region,bucket_name,s3_tmp_path,session)

    # download results to local
    s3_output_file = location.replace(f's3://{bucket_name}/', '')
    temp_result_location = f'/Users/perez/Downloads/{session_id}.csv'
    download_file(s3_output_file, bucket_name, temp_result_location)
    query_results_wrapper(drop_table_sql,region,bucket_name,s3_tmp_path,session)
    #s3_clean_up(bucket_name, s3_tmp_path)

    # generate gRNA dictionary for further processing
    gRNA_dict = convert_SQL_query_upload_process_download_file_to_gRNA_dictionary(temp_result_location)

    # time recording
    #end = time.time()
    #print(end - start)

    return gRNA_dict

#################
#               #
#   SQL Static  #
#               #
#################

region = 'us-west-2'
bucket_name = 'silico-palmsprings'
s3_tmp_path = 'CSC_website_directory/tmp_output/'
s3_input_temp_folder = 'CSC_website_directory/tmp_input/'

#####################
#                   #
#   Boot Up Loads   #
#                   #
#####################

###############
# FASTA Files #
###############

sys.stdout.write('FASTA files located\n')
hg38_fasta_file = '/Users/perez/Tools/genomes/hg38/genome_files/aggregate_hg38/all_genome_files/hg38_all.fa'
mm10_fasta_file = '/Users/perez/Tools/genomes/mm10/mm10_aggregate/mm10.fa'

############################################
# feature annotation dictionary generation #
############################################

sys.stdout.write('generating feature dictionaries\n')
hg38_genes = '/Users/perez/Tools/genome_annotations/hg38/annotation_bed_files/gene/hg38_annotation_gene.bed'
mm10_genes = '/Users/perez/Tools/genome_annotations/mm10/annotation_bed_files/genecode_vm25/mm10_genecode_vm25_knowncds_verbose_exon_coords_modified_for_intTree.bed'

hg38_annotation_file_lst = [hg38_genes]
mm10_annotation_file_lst = [mm10_genes]

hg38_feature_dict = feature_dictionary_generation(hg38_annotation_file_lst)
mm10_feature_dict = feature_dictionary_generation(mm10_annotation_file_lst)

########################################
# Safe Harbor Control Specificity Data #
########################################

hg38_safe_harbor_specificity_data = '/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/hg38_gRNA_lookup_new_delimiter_human_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'
mm10_safe_harbor_specificity_data = '/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/mm10_gRNA_lookup_new_delimiter_mouse_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'

###################
# hash table load #
###################

hg38_assembly_human_gRNA_NGG_cas9_hash_table = '/Users/perez/Projects/vidigal/crispr_screens/scripts/nci/scripts/CSC_public/CSC_beta_python3/csc_v2/screen_models/Hamming/avana_patched_Hamming_string.pl'
#hg38_assembly_human_gRNA_NGG_cas9_hash_table = '/Users/perez/Projects/vidigal/crispr_screens/data/genomewide_hash/hg38_hamming_dict.pl'
mm10_assembly_human_gRNA_NGG_cas9_hash_table = '/Users/perez/Projects/vidigal/crispr_screens/data/genomewide_hash/mm10_hamming_dict.pl'

# load pickle and generate dictionaries
sys.stdout.write('loading hashtables\n')
##hamming_dict_hg38 = load_pickle(hg38_assembly_human_gRNA_NGG_cas9_hash_table)
#hamming_dict_mm10 = load_pickle(mm10_assembly_human_gRNA_NGG_cas9_hash_table)
#hamming_dict_hg38 = load_pickle(mm10_assembly_human_gRNA_NGG_cas9_hash_table) #TODO testing

# translate hamming string
##hamming_string_dict_hg38 = translate_hamming_string('hg38',hamming_dict_hg38)
#hamming_string_dict_mm10 = translate_hamming_string('mm10',hamming_dict_mm10)

# load pickle, translate hamming string, and generate dictionaries
hamming_string_dict_hg38 = pickle_readin_w_string_translation(hg38_assembly_human_gRNA_NGG_cas9_hash_table)

"""sys.stdout.write('string translation hg38\n')
hamming_string_dict_hg38 = {}
for key in hamming_dict_hg38.keys():
    hamming_string_dict_hg38[key] = hamming_dict_hg38[key].split('_')"""

"""sys.stdout.write('string translation mm10\n')
hamming_string_dict_mm10 = {}
for key in hamming_dict['Hamming_string'].keys():
    hamming_string_dict_mm10[key] = hamming_dict['Hamming_string'][key].split('_')"""

#############
#           #
#   Class   #
#           #
#############

class Crispr:
    #def __init__(self,queries,genometag,library,sublibrary,display,datafile,hashtable,tmpdir=''): #,tooManyQueries=False):
    def __init__(self, queries, genometag, library, sublibrary, display, datafile, hashtable, specificity_matched_controls,percentage_of_library_as_controls,chunks,std,safeharbor_negative_control,target,tmpdir=''):
        """Main class to process user input and prepare results to show.


        genometag: str with short name of genome (e.g., "hg38" or "mm10") (I NEED)
        library: precomputed or custom
        sublibrary: (str)can choose between Avana, Brunello, GeckoV1, GeckoV2, TKOV3 or custom libraries
        
                  -pickle files
                  -strip empty lines
                  - 

        """
        self.queries = queries 
        self.genometag = genometag
        self.library = 'Custom'
        #self.library = library
        #self.sublibrary = sublibrary
        self.error = []      # where all errors will be stored
        self.warning = []    # where all warning will be stored
        self.results = []    # list of pairs
        self.metrics = []
        self.metricsWord = []
        self.display = display
        self.datafile = datafile
        self.tooManyQueries = False
        self.line_counter = 0
        self.RMSE = 1
        self.hashtable = hashtable
        self.specificity_matched_controls = specificity_matched_controls
        self.percentage_of_library_as_controls = percentage_of_library_as_controls
        self.chunks = chunks
        self.std = std
        self.safeharbor_negative_control = safeharbor_negative_control
        self.tmpdir = tmpdir
        self.target = target


    def calculate(self, ui_dict, tmpdir=""):

        print("inside calculate ",self.datafile)
        model_processed = False
        RMSEVal = 1
        if self.datafile:
            print("found datafile!")
            print(self.datafile)
            if(self.library == "Custom"):
                print("in custom")
                #processing(self.datafile, '/Users/perez/Projects/vidigal/crispr_screens/data/genomewide_hash/hg38_hamming_dict.pl','g',self.tmpdir,1.0,20)
                error_process = csc_processing(self.datafile,self.hashtable, self.tmpdir, 1.0, 20,self.specificity_matched_controls,self.percentage_of_library_as_controls,self.chunks,self.std,self.safeharbor_negative_control,ui_dict,self.target) #csc_processing(in_file, hamming_string_dict,outdir,rsm_value,gRNA_length)

                if error_process == -1:
                    return error_process

                pastLine = False
                pastRMSE = False
                if (path.exists(self.tmpdir + '/csc_model_metrics_Upload.txt')):
                    with open(self.tmpdir + '/csc_model_metrics_Upload.txt', 'r') as file:
                        for line2 in file:
                            str2 = line2
                            print("Str2: " + str2)
                            if ("MSE:" in str2):
                                pastLine = True
                            if (pastLine):
                                self.metrics.append(str2)
                                for word2 in str2.split():
                                    self.metricsWord.append(word2)
                                    if (len(self.metricsWord) == 23):
                                        RMSEVal = float(word2)
                                    if ("Predictions:" in word2):
                                        pastRMSE = True
                else:
                    if self.specificity_matched_controls == -1:
                        self.results.append("1COL_no_specificity")
                        self.metrics.append("1COL_no_specificity")
                    else:
                        self.results.append("1COL")
                        self.metrics.append("1COL")

                if (RMSEVal < 1):
                    with open(self.tmpdir + '/csc_output_Upload_earth_patched.csv', 'r') as file:
                        while len(self.results) < 70:
                            for line in file:
                                str = line.replace(",", " ")
                                for word in str.split():
                                    self.results.append(word)

                else:
                    with open(self.tmpdir + '/Upload_CSC_gRNA_Hamming_neighborhood.csv', 'r') as file:
                        self.results.append(">1")
                        while len(self.results) < 60:
                            for line in file:
                                str = line.replace(",", " ")
                                for word in str.split():
                                    self.results.append(word)

                    file.close()

                self.RMSE = RMSEVal
                print("here!")
                return error_process
                
            else:
                #processing(self.datafile,self.sublibrary,'l',self.tmpdir,1.0,20)
                error_process = csc_processing(self.datafile, self.hashtable, self.tmpdir, 1.0, 20, self.specificity_matched_controls,
                               self.percentage_of_library_as_controls, self.chunks, self.std,
                               self.safeharbor_negative_control,ui_dict,self.target)  # csc_processing(in_file, hamming_string_dict,outdir,rsm_value,gRNA_length)

                if error_process == -1:
                    return error_process
                else:
                    pass

                pastLine = False
                pastRMSE = False
                if (path.exists(self.tmpdir + '/csc_model_metrics_Upload.txt')):
                    with open(self.tmpdir + '/csc_model_metrics_Upload.txt', 'r') as file:
                        for line2 in file:
                            str2 = line2
                            print("Str2: " + str2)
                            if("MSE:" in str2):
                                pastLine = True
                            if(pastLine):
                                self.metrics.append(str2)
                                for word2 in str2.split():
                                    self.metricsWord.append(word2)
                                    if(len(self.metricsWord) == 23):
                                        RMSEVal = float(word2)
                                    if("Predictions:" in word2):
                                        pastRMSE = True
                else:
                    if self.specificity_matched_controls == -1:
                        self.results.append("1COL_no_specificity")
                        self.metrics.append("1COL_no_specificity")
                    else:
                        self.results.append("1COL")
                        self.metrics.append("1COL")

                if(RMSEVal < 1):
                    with open(self.tmpdir + '/csc_output_Upload_earth_patched.csv', 'r') as file:
                        while len(self.results) < 70:
                            for line in file:
                                str = line.replace(",", " ")
                                for word in str.split():
                                    self.results.append(word)

                else:
                    with open(self.tmpdir + '/Upload_CSC_gRNA_Hamming_neighborhood.csv', 'r') as file:
                        self.results.append(">1")
                        while len(self.results) < 60:
                            for line in file:
                                str = line.replace(",", " ")
                                for word in str.split():
                                    self.results.append(word)

                    file.close()

                self.RMSE = RMSEVal
                print("here!")

            print("finished adjustment")
            return error_process

    def process_input(self, inputs):
        splitInput = inputs.splitlines()
        splitInput = [s.strip() for s in splitInput]
        return splitInput

class TemporaryFileStorage(cherrypy._cpreqbody.Part):
    """custom entity part that will always create a named temporary file for the entities (stackoverflow)"""
    maxrambytes = 0

    def make_file(self):
        return tempfile.NamedTemporaryFile()

class Root(object):
    def __init__(self):
        print('Loading Site...')
    
    @cherrypy.expose
    def index(self, **params):
        """
        set up site
        """
        print('In index')
    
        tmpl = env.get_template('crispr.html')
        #tmpl = env.get_template('csc_template.html')

        # safe harbor specificity matched controls files
        self.safeharbor_negative_control_hg38 = hg38_safe_harbor_specificity_data #'/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/hg38_gRNA_lookup_new_delimiter_human_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'
        self.safeharbor_negative_control_mm10 = mm10_safe_harbor_specificity_data #'/Users/perez/Projects/vidigal/guidescan_screens/data/safeharbor_enumerates/mm10_gRNA_lookup_new_delimiter_mouse_safe_harbor_coordinates.txt_trie_based_specificity_scores_ClassTask.txt'

        # feature coordinate dictionaries
        self.hg38_feature_dictionary = hg38_feature_dict
        self.mm10_feature_dictionary = mm10_feature_dict

        self.genome = "hg38"
        self.library = "Custom"
        #self.library = "precomputed"
        self.sublibrary = None #avana
        self.display='all'
        self.datafile=''
        self.tooManyQueries=False
        self.enzyme="cas9"
        self.RMSE = 1 #changed from 0 to 1
        self.hashtable_hg38 = None #hamming_string_dict_hg38
        #self.hashtable_mm10 = hamming_string_dict_mm10

        # specificity matching
        self.specificity_matched_controls = 0
        self.chunks = 20
        self.std = 1
        self.percentage_of_library_as_controls = .1

        # targetting window
        self.target = 'within_without_specificity_controls'
        self.flank_window = 1000
        
        acceptable_characters = string.ascii_letters + string.digits + string.whitespace + '#:-_.*'
        #queries = ''.join(character for character in queries if character in acceptable_characters)
        #self.kmer = ''.join(character for character in self.kmer if character in acceptable_characters)
        self.genome = ''.join(character for character in self.genome if character in acceptable_characters)
        self.target = ''.join(character for character in self.target if character in acceptable_characters)
        self.library = ''.join(character for character in self.library if character in acceptable_characters)
        #self.sublibrary = ''.join(character for character in self.sublibrary if character in acceptable_characters)
        #self.mode = ''.join(character for character in self.mode if character in acceptable_characters)
        #self.flank_size = ''.join(character for character in self.flank_size if character in acceptable_characters)
        #self.ordering = ''.join(character for character in self.ordering if character in acceptable_characters)
        #self.topN = ''.join(character for character in self.topN if character in acceptable_characters)
        self.display = ''.join(character for character in self.display if character in acceptable_characters)
        self.enzyme = ''.join(character for character in self.enzyme if character in acceptable_characters)
        #self.RMSE = ''.join(character for character in self.enzyme if character in acceptable_characters)
     
        return tmpl.render(RMSE = self.RMSE, genome = self.genome, library = self.library, sublibrary = self.sublibrary, display=self.display, datafile=self.datafile,enzyme=self.enzyme,hashtable_hg38=self.hashtable_hg38, target=self.target)
 
    @cherrypy.expose
    def show_results(self,datafile = None, queries="", library = "Custom", sublibrary = "Avana", display = 'all', RMSE =0,**params): #removed genome="hg38",

        self.enzyme = cherrypy.request.params.get('enzyme')
        self.genome = cherrypy.request.params.get('genome')
        self.target = cherrypy.request.params.get('target')

        # Enzyme Selection
        if self.enzyme == 'cas9':
            # coordinate to gRNA lookup
            self.total_string_length = 23
            self.nonvariable_fixed_pam_length = 2
            self.nonvariable_fixed_forward_pam_sequence = 'GG'
            self.nonvariable_fixed_reverse_pam_sequence = 'CC'
            self.direction_5_or_3_prime = 3
            self.wildcard_position = 3
        elif self.enzyme == 'cas12a':
            # coordinate to gRNA lookup
            self.total_string_length = 26
            self.nonvariable_fixed_pam_length = 3
            self.nonvariable_fixed_forward_pam_sequence = 'TTT'
            self.nonvariable_fixed_reverse_pam_sequence = 'AAA'
            self.direction_5_or_3_prime = 5
            self.wildcard_position = 4

        # Design Selection
        if self.target == 'within_without_specificity_controls':
            self.specificity_matched_controls = -1
        elif self.target == 'flanking_without_specificity_controls':
            self.specificity_matched_controls = -1
        else:
            self.specificity_matched_controls = 0

        print("** Upload Query **")
        try:
            datafile.file.seek(0)
        except AttributeError:
            sys.stdout.write('site ran without upload file\n')
            tmpl = env.get_template('crispr.html')
            return tmpl.render(RMSE=self.RMSE, genome=self.genome, library=self.library, sublibrary=self.sublibrary,
                               display=self.display, datafile=self.datafile, enzyme=self.enzyme,
                               hashtable_hg38=self.hashtable_hg38)

        tmpdir=tempfile.mkdtemp()
        #dfname=tmpdir + '/Upload.txt'
        dfname = tmpdir + '/Upload_preprocess.txt'

        print("DFname: " + dfname)
        print('Enzyme = %s\n' % self.enzyme)
        print('Genome = %s\n' % self.genome)
        print('Target = %s\n' % self.target)
        print('Specificity Parameter = %s\n' % self.specificity_matched_controls)

        df = open(dfname,'wb')
        df.write(datafile.file.read())
        df.close()

        # process upload_preprocess file
        if self.genome == 'hg38':
            fasta_file = hg38_fasta_file
            feature_dictionary = self.hg38_feature_dictionary
        elif self.genome == 'mm10':
            fasta_file = mm10_fasta_file
            feature_dictionary = self.mm10_feature_dictionary
        else:
            sys.stdout.write('Non-supported genome %s\n' % self.genome)
            tmpl = env.get_template('crispr.html')
            return tmpl.render(RMSE=self.RMSE, genome=self.genome, library=self.library, sublibrary=self.sublibrary,
                               display=self.display, datafile=self.datafile, enzyme=self.enzyme,
                               hashtable_hg38=self.hashtable_hg38)

        # TODO flanking option here, add to lookup_processing
        ui_dict = csc_gRNA_lookup_processing(dfname,fasta_file,tmpdir,self.total_string_length,self.nonvariable_fixed_pam_length,self.nonvariable_fixed_forward_pam_sequence,self.nonvariable_fixed_reverse_pam_sequence,self.direction_5_or_3_prime,self.wildcard_position,self.target,feature_dictionary)

        post_process_dfname = tmpdir + '/Upload.txt'

        # SQL Database query for gRNA specificity metrics
        #gRNA_dict = user_gRNA_upload_to_SQL_database_query_for_gRNA_specificity_metrics_and_dict_creation(post_process_dfname, region, bucket_name, s3_tmp_path)
        gRNA_dict = upload_user_file_for_SQL_database_query_and_pull_resulting_gRNAs_to_make_gRNA_specificity_dict(post_process_dfname, region, bucket_name, s3_tmp_path, s3_input_temp_folder)

        #TODO modify so that not hardcoded to hg38
        if self.genome == 'hg38':
            self.hashtable_hg38 = gRNA_dict

        tmpl = env.get_template('crispr.html')
        
        #if (library == "Custom"): #TODO mm10
        #    sublibrary = None
        if self.genome == 'hg38':
            h = self.hashtable_hg38
            safe_harbor = self.safeharbor_negative_control_hg38
        elif self.genome == 'mm10':
            h = self.hashtable_mm10
            safe_harbor = self.safeharbor_negative_control_mm10
        else:
            sys.stdout.write('Non-supported genome %s\n' % self.genome)
            tmpl = env.get_template('crispr.html')
            return tmpl.render(RMSE=self.RMSE, genome=self.genome, library=self.library, sublibrary=self.sublibrary,
                               display=self.display, datafile=self.datafile, enzyme=self.enzyme,
                               hashtable_hg38=self.hashtable_hg38)

        print("Genome: " + self.genome)
        print("Library: " + library)
  
        crispr = Crispr(queries = '',
                          genometag = self.genome, library = library, sublibrary = sublibrary, display=display, datafile=post_process_dfname,hashtable=h,specificity_matched_controls=self.specificity_matched_controls,percentage_of_library_as_controls=self.percentage_of_library_as_controls,chunks=self.chunks,std=self.std,safeharbor_negative_control=safe_harbor,target=self.target,tmpdir=tmpdir)

        # TODO allow coordinate to gRNA conversion here
        error_process = crispr.calculate(ui_dict)

        if error_process == -1:
            # restore default settings
            tmpl = env.get_template('crispr_error.html')
            return tmpl.render(error=crispr.error, warning=crispr.warning, result=crispr.results,
                               metrics=crispr.metrics, metricsWord=crispr.metricsWord, queries=crispr.queries,
                               genometag=self.genome, library=crispr.library,
                               display=crispr.display,
                               datafile=crispr.datafile, tmpdir=crispr.tmpdir, RMSE=crispr.RMSE)

        cherrypy.log(cherrypy.request.remote.ip + "\t" + str(len(crispr.results)) + " results")

        if os.path.exists(crispr.datafile):
            print('%s EXISTS! ' % (df.name))

        if os.path.exists(crispr.tmpdir):
            print('crispr.tmpdir is %s' % (crispr.tmpdir))

        else:
            os.listdir(crispr.tmpdir)

        gc.collect()

        # restore default settings
        return tmpl.render(error=crispr.error, warning=crispr.warning,result=crispr.results, metrics = crispr.metrics, metricsWord = crispr.metricsWord,queries=crispr.queries, 
                             genometag = self.genome, library = crispr.library,
                             display=crispr.display,
                             datafile=crispr.datafile,tmpdir=crispr.tmpdir, RMSE = crispr.RMSE)

    @cherrypy.expose
    def download_results(self,tmpdir="", RMSE = ""):

        if(float(RMSE) < 1):
            print("in download: " + RMSE)
            print("tmpdir: " + tmpdir + '/csc_output_Upload_earth_patched.csv')
            file = tmpdir + '/csc_output_Upload_earth_patched.csv' ###download###

        else:
            file = tmpdir + '/Upload_CSC_gRNA_Hamming_neighborhood.csv'

        return serve_file(file,"application/x-download", "attachment")
   
    @cherrypy.expose
    def download_results_metrics(self,tmpdir=""):
       
        print("tmpdir: " + tmpdir + '/csc_model_metrics_Upload.txt') ###Downloads###
        file= tmpdir + '/csc_model_metrics_Upload.txt'
        return serve_file(file,"application/x-download", "attachment")

    @cherrypy.expose
    def download_results_specificity_matching(self, tmpdir=""):

        print("tmpdir: " + tmpdir + '/specificity_matched_controls.csv')  ###Downloads###
        file = tmpdir + '/specificity_matched_controls.csv'
        return serve_file(file, "application/x-download", "attachment")

    @cherrypy.expose
    def results_specificity_plot(self, tmpdir=""):

        print("tmpdir: " + tmpdir + '/cdf_Upload_CSC_gRNA_Hamming_neighborhood.csv.png')  ###Downloads###
        file = tmpdir + '/cdf_Upload_CSC_gRNA_Hamming_neighborhood.csv.png'
        return serve_file(file, "application/x-download", "attachment")

    @cherrypy.expose()
    def help(self, **params):
        tmpl_help = env.get_template('help.html')
        return tmpl_help.render()

    @cherrypy.expose()
    def contact(self, **params):
        tmpl_contact = env.get_template('contact.html')
        return tmpl_contact.render()
    

cherrypy.tools.secureheaders = cherrypy.Tool('before_finalize', secureheaders, priority=60)
cherrypy.tools.noBodyProcess = cherrypy.Tool('before_request_body',noBodyProcess)


cherrypy.server.socket_timeout = 3600 
cherrypy.config.update({'server.socket_port': 8080}) # 8080 or 80
print("Here: " +current_dir + "/static/")

conf = {'/':

                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': current_dir + "/static/",
                 'tools.secureheaders.on': True,
                 'request.body.part_class':TemporaryFileStorage
                },
                
       'global':    
                { 'server.environment': 'production',
                  'engine.autoreload.on': True,
                  'engine.autoreload.frequency': 5,
                  'server.socket_host': '127.0.0.1',
                  'server.socket_port': 8080, #8080 or 80
                  'server.thread_pool': 30,
                  'log.access_file': "",
                  'log.error_file': "",
                  'request.show_tracebacks':False
                },
        }

if __name__ == '__main__':
    cherrypy.quickstart(Root(), '/', config= conf)
