import numpy
import Bio
from Bio import Entrez
from Bio import SeqIO
from Bio import Seq
import pandas
import re
import ipdb


def refseq_prepare(ids, email='lef73@cornell.edu'):
    """
    refseq ids are inputted,
    retrieves sequences,
    tiles sequences into 22-mers,
    generates names
    """
    
    Entrez.email = email
    geneID = getName(ids)
    handle2 = Entrez.efetch(db="nucleotide", id=ids, rettype="fasta", retmode="text")

    for sequence in SeqIO.parse(handle2,"fasta"):
        string = str(sequence.seq)

        tiled = []
        for i in range(0,len(string)-21):
            tile = string[i:i+22]
            rc = str(Seq.Seq(tile).reverse_complement())
            tiled.append(rc)
        handle2.close()

        names = []
        for i in range(len(tiled)):
            name = ids + '_' + str(i)
            names.append(name)

    return tiled, names, geneID, string


def query_Entrez(gene_id, email='lef73@cornell.edu'):
	Entrez.email=email
	try:
		entrezInfo = Entrez.efetch(db='gene', id=gene_id, retmode='text').read()
		result = entrezInfo.split("\n")
	except:
		result = "Entrez gene information could not be retrieved"
	return result



def get_seq_names(label, gene_id, idx, unionPos="", interPos=""):
    names = []
    for i in idx:
        if(interPos == "" or unionPos == ""):
            name = str(gene_id) + "_" + str(i)
        else:
            genomic_pos = interPos[int(i)]
            union_pos = unionPos[1].index(genomic_pos)
            if (unionPos[0] == '-'):
                union_pos = len(unionPos[1]) - union_pos
            name = label + "_" + str(gene_id) + '_' + str(union_pos)			# str(j) = position in intersection
        names.append(name)
    return names


# Create tiled sequences from full sequence
def entrezID_prepare(ids, gene_seq, gene_id, unionPos="", interPos=""):
    if (gene_id != ""):
        entrez_info = query_Entrez(gene_id)
    else:
        gene_id = "Sequence"
        entrez_info = ""

    # tile sequence
    tiled = []
    if gene_seq != "":
        gene_seq = gene_seq.upper()
        for j in range(0,len(gene_seq)-21):
            tile = gene_seq[j:j+22]
            if "*" in tile:
                pass
            else:
                rc = str(Seq.Seq(tile).reverse_complement())
                tiled.append(rc)
    else:
        print("empty string")

    names = get_seq_names(ids, gene_id, range(len(tiled)), unionPos, interPos)
    
    return tiled, names, entrez_info



def seq_prepare(ids, seqs):
    biglist = []
    namelist = []
    if (isinstance(seqs, str)):
	seqs = [seqs]
	
    for seq in seqs:
        tiled = []
        if seq != []:
            seq = seq.upper()
            for i in range(0,len(seq)-21):
                tile = seq[i:i+22]
                if "*" in tile:
                    pass
                else:
                    rc = str(Seq.Seq(tile).reverse_complement())
                    tiled.append(rc)
        else:
            print("empty string")
        biglist.append(tiled)

    names = []
    if len(tiled) > 0:
        for j in range(len(tiled)):
    		name = "Sequence_" + str(j)
		names.append(name)
        namelist.append(names)
    else:
        namelist.append([])
        
    return biglist,namelist
	





def getName(id, email="lef73@cornell.edu"):
	Entrez.email=email
	out =  Entrez.efetch(db="nucleotide", id=id,retmode="text").read()
	out = out.split('\n')
	prNext = ""
	string = ""

	for line in out:
	    if prNext != "":
		if (line.strip().find("title") != -1):
		    string = line.strip()
		    string = string.rstrip(',')
		    string = string.lstrip("title")
		    string = string.strip()
		    string = string.strip("\"")
		prNext = ""
	    
	    if (line.strip().find("descr {") == 0):# and str.find("(") != -1):
		prNext = line.strip()
	if string == "":
		return "Error retrieving NCBI details"
	else:
		return string










