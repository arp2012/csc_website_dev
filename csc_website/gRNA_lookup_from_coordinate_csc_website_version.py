__author__ = 'Alexendar Perez'

#####################
#                   #
#   Introduction    #
#                   #
#####################

"""look up gRNA from coordinates for csc website processing"""

#################
#               #
#   Libraries   #
#               #
#################

import sys
import argparse

import numpy as np
import pandas as pd
from Bio import Seq
from pyfaidx import Fasta

#########################
#                       #
#   Auxillary Function  #
#                       #
#########################

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--infile',help='absolute filepath to input file',required=True)
    parser.add_argument('-f','--fasta',help='absolute filepath to fasta file in same directory as its index',required=True)
    parser.add_argument('-o','--outdir',help='absolute filepath to output directory',required=True)
    parser.add_argument('--total_string_length',help='length of gRNA with PAM, default = 23',default=23)
    parser.add_argument('--nonvariable_fixed_pam_length',help='length of conserved PAM sequence in NGG this is GG which equals 2, default = 2',default=2)
    parser.add_argument('--nonvariable_fixed_forward_pam_sequence',help='conserved forward PAM sequence, in NGG this is GG, default = GG',default='GG')
    parser.add_argument('--nonvariable_fixed_reverse_pam_sequence',help='conserved reverse PAM sequence, in NGG this is CC, default = CC',default='CC')
    parser.add_argument('--direction_5_or_3_prime',help='direction of PAM sequence, for NGG Cas9 this is 3 prime, default = 3',default=3)
    parser.add_argument('--wildcard_position',help='position of wildcard sequence, in NGG this is 3, default = 3',default=3)
    parser.add_argument('--feature_dictionary',help='dictionary object with feature names as key and chr:start-stop as values, default = None',default=None)

    args = parser.parse_args()
    infile = args.infile
    fasta = args.fasta
    outdir = args.outdir
    total_string_length = args.total_string_length
    nonvariable_fixed_pam_length = args.nonvariable_fixed_pam_length
    nonvariable_fixed_forward_pam_sequence = args.nonvariable_fixed_forward_pam_sequence
    nonvariable_fixed_reverse_pam_sequence = args.nonvariable_fixed_reverse_pam_sequence
    direction_5_or_3_prime = args.direction_5_or_3_prime
    wildcard_position = args.wildcard_position
    feature_dictionary = args.feature_dictionary

    return infile,fasta,outdir,int(total_string_length),int(nonvariable_fixed_pam_length),nonvariable_fixed_forward_pam_sequence,nonvariable_fixed_reverse_pam_sequence,int(direction_5_or_3_prime),int(wildcard_position),feature_dictionary

def csc_gRNA_lookup_processing(input_file, fasta_file, outdir, total_string_length, nonvariable_fixed_pam_length,
               nonvariable_fixed_forward_pam_sequence, nonvariable_fixed_reverse_pam_sequence, direction_5_or_3_prime,
               wildcard_position,target,feature_dictionary):
    # fasta file lookup
    msg, org = fasta(fasta_file)
    sys.stdout.write(msg)

    # read in file
    dataframe, classification = read_in(input_file)

    # process lines in dataframe following conversion to array
    data_array = np.asarray(dataframe)

    # process lines in file to determine coordinates and gRNA sequence
    ui_dict = gRNA_file_generation_for_csc_website_processing(outdir, data_array, total_string_length,
                                                    nonvariable_fixed_pam_length,
                                                    nonvariable_fixed_forward_pam_sequence,
                                                    nonvariable_fixed_reverse_pam_sequence, wildcard_position,
                                                    direction_5_or_3_prime,org,target,feature_dictionary)

    return ui_dict

def fasta(fasta_file):
    """Load organism fasta file for use in pyfaidx module

    Args:
    fasta_file = the full filepath, including the file itself, to the organism's fasta file

    Note: an index of the fasta file should also be present in the same directory. This can
          be produced using samtools faidx command and will have the suffix .fai
    """
    org = Fasta(fasta_file)
    return ('%s accessed\n' % fasta_file),org

def read_in(in_file):
    """multiple attempt read in for generic file

    :param in_file: absolute filepath to input file
    :return: opened file, classification of opening method

    """
    classification = '.csv'
    if '\t' in open(in_file).readline():
        classification = '.txt'

    try:
        infile = pd.read_excel(in_file,header=None)
        sys.stdout.write('file read in as Excel\n')
        classification = 'excel'

    except:

        try:
            if classification == '.csv':
                infile = pd.read_csv(in_file,header=None)
                sys.stdout.write('file read in as csv\n')
            else:
                infile = pd.read_csv(in_file, sep='\t',header=None)
                sys.stdout.write('file read in as txt\n')

        except:
            infile = pd.DataFrame(open(in_file, 'r'))
            sys.stdout.write('file read in with python open function and cast as pandas DataFrame\n')
            classification = 'readin'

    return infile,classification

def string_lookup_from_coordinate(chromosome, start, stop, org):
    """lookup genomic string from coordinates

    :param chromosome: string value, chromosome name
    :param start: int value, start coordinate of string in genome with 0-based index
    :param stop: int value, stop coordinate of string in genome with 0-based index
    :param org: second output of fasta()
    :return: lookup string from coordinate

    """
    try:
        string = str(org[chromosome][int(start):int(stop)]).upper()

    except KeyError:
        if chr == 'M':
            try:
                string = str(org['MT'][int(start):int(stop)]).upper()
            except:
                sys.stderr.write('Mitoclondrial bypass failed on %s, skipping\n' % chr)
                string = -1
        else:
            sys.stderr.write('chromosome key error %s, skipping\n' % chr)
            string = -1

    return string

def n_placement(input_string,wildcard_position,direction_5_or_3_prime):
    """places wildcard character N at flexible position in PAM sequence

    :param input_string: full string
    :param wildcard_position: int value, position where wildcard exists, such as N in NGG for cas9
    direction_5_or_3_prime: int value, either 5 or 3 for 5prime or 3prime. Cas9 is a 3prime PAM and Cas12a/CPFA is 5prime
    :return: string with wildcard placement

    """
    string_lst = list(input_string)

    if wildcard_position != 0:
        if direction_5_or_3_prime == 3:
            wildcard = -wildcard_position
        elif direction_5_or_3_prime == 5:
            wildcard = wildcard_position
        else:
            sys.stderr.write('assume 3 prime wildcard position (like NGG in Cas9), %s not valid, enter either 3 or 5\n' % direction_5_or_3_prime)

        string_lst[wildcard] = 'N'
        output_string = ''.join(string_lst)

    else:
        output_string = ''.join(string_lst)

    return output_string

def flanking_query_coordinate_adjustment(start, stop, flank_window):
    """flanking coordinate adjustment for gRNA query

    :param start: int value, initial start coordinate
    :param stop: int value, initial stop coordinate
    :param flank_window: int value, value of coordinate adjustment (ie: 1000 mean shift start left 1000, stop right 1000)
    :return: flanking adjusted coordinates: flanking_start,start,stop,flanking_stop

    """
    return int(start) - flank_window, start, stop, int(stop) + flank_window

def extract_gRNA_from_string(outfile, outfile_verbose, string, total_string_length, nonvariable_fixed_pam_length,
                             nonvariable_fixed_forward_pam_sequence, nonvariable_fixed_reverse_pam_sequence,
                             wildcard_position, direction_5_or_3_prime, start, stop, chromosome,target_flag,ui_dict):

    count = 0
    for i in range(len(string)):
        substring = string[i:i + total_string_length]

        if len(substring) == total_string_length:
            if substring[-nonvariable_fixed_pam_length:] == nonvariable_fixed_forward_pam_sequence:
                select, strand = n_placement(substring, wildcard_position, direction_5_or_3_prime), '+'

                string_start, string_stop = start + i + 1, start + i + total_string_length

                outfile_verbose.write('%s:%s-%s,%s:%s-%s,%s,%s,%s:%s-%s_%s_%s\n' % (
                chromosome, start, stop, chromosome, string_start, string_stop, strand, select,chromosome,start,stop,target_flag,count))

                upload_prcessing_select = select.split('N')[0]

                outfile.write('%s\n' % upload_prcessing_select)

                ui_dict[select] = '%s:%s-%s_%s_%s' % (chromosome,start,stop,target_flag,count)
                count += 1

            elif substring[:nonvariable_fixed_pam_length] == nonvariable_fixed_reverse_pam_sequence:
                select, strand = n_placement(str(Seq.Seq(substring).reverse_complement()), wildcard_position,
                                             direction_5_or_3_prime), '-'

                string_start, string_stop = start + i + 1, start + i + total_string_length

                outfile_verbose.write('%s:%s-%s,%s:%s-%s,%s,%s,%s:%s-%s_%s_%s\n' % (
                    chromosome, start, stop, chromosome, string_start, string_stop, strand, select, chromosome, start,
                    stop, target_flag, count))

                upload_prcessing_select = select.split('N')[0]

                outfile.write('%s\n' % upload_prcessing_select)

                ui_dict[select] = '%s:%s-%s_%s_%s' % (chromosome, start, stop, target_flag, count)
                count += 1

            else:
                continue

        else:
            continue

    return ui_dict

def gRNA_file_generation_for_csc_website_processing(outdir,data_array,total_string_length,nonvariable_fixed_pam_length,nonvariable_fixed_forward_pam_sequence,nonvariable_fixed_reverse_pam_sequence,wildcard_position,direction_5_or_3_prime,org,target,feature_dictionary):
    """extract gRNAs from coordinates for use in CSC website processing

    :param outdir: string value, absolute filepath to output directory
    :param data_array: numpy array, array format of input file which allows for line by line processing
    :param total_string_length: int value, length of gRNA with its PAM
    :param nonvariable_fixed_pam_length: int value, length of conserved segments of PAM: NGG has length 2 since GG conserved
    :param nonvariable_fixed_forward_pam_sequence: string value, forward version of conserved PAM: NGG would be GG
    :param nonvariable_fixed_reverse_pam_sequence: string value, reverse version of conserved PAM: NGG would be CC
    :param wildcard_position: position of N wild card: NGG would be 3 [-3 more specificially, but negative accounted for in func]
    :param direction_5_or_3_prime: int value, 5 prime or 3 prime location of PAM: NGG is 3prime PAM
    :param org: second output of fasta()
    :return: verbose and upload ready files

    """
    #TODO allow flanking lookup
    flank_window = 1000
    ui_dict = {}
    with open('%s/Upload_verbose.txt' % outdir, 'w') as outfile_verbose:
        with open('%s/Upload.txt' % outdir,'w') as outfile:
            for i in data_array:
                i = [item for item in i if not(pd.isnull(item))==True]
                if len(i) >= 3:
                    chromosome,start,stop = i[0],int(i[1]),int(i[2]) #bedfile [chrom    start    stop]
                    if target == 'within_without_specificity_controls' or target == 'within_with_specificity_controls':
                        string = string_lookup_from_coordinate(chromosome, start, stop, org)

                        ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string, total_string_length, nonvariable_fixed_pam_length,
                                                 nonvariable_fixed_forward_pam_sequence, nonvariable_fixed_reverse_pam_sequence,
                                                 wildcard_position, direction_5_or_3_prime,start, stop, chromosome,'within',ui_dict)

                    else:
                        left_flank_start, left_flank_stop, right_flank_start, right_flank_stop = flanking_query_coordinate_adjustment(
                            start, stop, flank_window)
                        string_left = string_lookup_from_coordinate(chromosome, left_flank_start, left_flank_stop, org)
                        string_right = string_lookup_from_coordinate(chromosome, right_flank_start, right_flank_stop,
                                                                     org)

                        ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string_left, total_string_length,
                                                 nonvariable_fixed_pam_length,
                                                 nonvariable_fixed_forward_pam_sequence,
                                                 nonvariable_fixed_reverse_pam_sequence,
                                                 wildcard_position, direction_5_or_3_prime, left_flank_start, left_flank_stop, chromosome,'left_flank',ui_dict)

                        ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string_right, total_string_length,
                                                 nonvariable_fixed_pam_length,
                                                 nonvariable_fixed_forward_pam_sequence,
                                                 nonvariable_fixed_reverse_pam_sequence,
                                                 wildcard_position, direction_5_or_3_prime, right_flank_start, right_flank_stop, chromosome,'right_flank',ui_dict)

                elif len(i) == 1:
                    try:
                        chromosome,start,stop = i[0].split(':')[0],int(i[0].split(':')[1].split('-')[0]),int(i[0].split(':')[1].split('-')[1]) # example ['chr17:48377262-48380370']
                        if target == 'within_without_specificity_controls' or target == 'within_with_specificity_controls':
                            string = string_lookup_from_coordinate(chromosome, start, stop, org)

                            ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string, total_string_length,
                                                     nonvariable_fixed_pam_length,
                                                     nonvariable_fixed_forward_pam_sequence,
                                                     nonvariable_fixed_reverse_pam_sequence,
                                                     wildcard_position, direction_5_or_3_prime, start, stop, chromosome,'within',ui_dict)

                        else:
                            left_flank_start,left_flank_stop,right_flank_start,right_flank_stop = flanking_query_coordinate_adjustment(start, stop, flank_window)
                            string_left = string_lookup_from_coordinate(chromosome, left_flank_start, left_flank_stop, org)
                            string_right = string_lookup_from_coordinate(chromosome, right_flank_start, right_flank_stop,
                                                                        org)

                            ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string_left, total_string_length, nonvariable_fixed_pam_length,
                                                     nonvariable_fixed_forward_pam_sequence, nonvariable_fixed_reverse_pam_sequence,
                                                     wildcard_position, direction_5_or_3_prime,left_flank_start, left_flank_stop, chromosome,'left_flank',ui_dict)

                            ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string_right, total_string_length,
                                                     nonvariable_fixed_pam_length,
                                                     nonvariable_fixed_forward_pam_sequence,
                                                     nonvariable_fixed_reverse_pam_sequence,
                                                     wildcard_position, direction_5_or_3_prime, right_flank_start, right_flank_stop, chromosome,'right_flank',ui_dict)


                    except IndexError: #TODO annotation to coordinate conversion
                        try:
                            sys.stdout.write('Attempt feature lookup\n')
                            if feature_dictionary == None:
                                sys.stdout.write(
                                    'Index Error: will assume %s is gRNA sequence and continue processing\n' % i)
                                sequence = i[0]
                                outfile_verbose.write('%s\n' % sequence)
                                upload_sequence = sequence.split('N')[0]
                                if len(upload_sequence) == 20:
                                    outfile.write('%s\n' % upload_sequence)

                                ui_dict[sequence] = 'gRNA_supplied_by_user'
                            else:
                                feature_coordinate = feature_dictionary[i[0]]
                                sys.stdout.write('%s\t%s\n' % (i[0],feature_coordinate))
                                chromosome, start, stop = feature_coordinate.split(':')[0], int(
                                    feature_coordinate.split(':')[1].split('-')[0]), int(
                                    feature_coordinate.split(':')[1].split('-')[1])  # example ['chr17:48377262-48380370']
                                if target == 'within_without_specificity_controls' or target == 'within_with_specificity_controls':
                                    string = string_lookup_from_coordinate(chromosome, start, stop, org)

                                    ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string, total_string_length,
                                                             nonvariable_fixed_pam_length,
                                                             nonvariable_fixed_forward_pam_sequence,
                                                             nonvariable_fixed_reverse_pam_sequence,
                                                             wildcard_position, direction_5_or_3_prime, start, stop,
                                                             chromosome,'within',ui_dict)

                                else:
                                    left_flank_start, left_flank_stop, right_flank_start, right_flank_stop = flanking_query_coordinate_adjustment(
                                        start, stop, flank_window)
                                    string_left = string_lookup_from_coordinate(chromosome, left_flank_start,
                                                                                left_flank_stop, org)
                                    string_right = string_lookup_from_coordinate(chromosome, right_flank_start,
                                                                                 right_flank_stop,
                                                                                 org)

                                    ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string_left, total_string_length,
                                                             nonvariable_fixed_pam_length,
                                                             nonvariable_fixed_forward_pam_sequence,
                                                             nonvariable_fixed_reverse_pam_sequence,
                                                             wildcard_position, direction_5_or_3_prime, left_flank_start, left_flank_stop,
                                                             chromosome,'left_flank',ui_dict)

                                    ui_dict = extract_gRNA_from_string(outfile, outfile_verbose, string_right,
                                                             total_string_length,
                                                             nonvariable_fixed_pam_length,
                                                             nonvariable_fixed_forward_pam_sequence,
                                                             nonvariable_fixed_reverse_pam_sequence,
                                                             wildcard_position, direction_5_or_3_prime, right_flank_start, right_flank_stop,
                                                             chromosome, 'right_flank',ui_dict)

                        except KeyError:
                            sys.stdout.write('Index Error: will assume %s is gRNA sequence and continue processing\n' % i)
                            sequence = i[0]
                            outfile_verbose.write('%s\n' % sequence)
                            upload_sequence = sequence.split('N')[0]
                            if len(upload_sequence) == 20:
                                outfile.write('%s\n' % upload_sequence)

                else:
                    sys.stdout.write('Two columns detected: will not alter, will proceed to CSC correction') # gRNA    logfc
                    sequence,metric = i[0],i[1]
                    outfile_verbose.write('%s\t%s\n' % (sequence,metric))
                    outfile.write('%s\t%s\n' % (sequence,metric))

            return ui_dict

#####################
#                   #
#   Main Function   #
#                   #
#####################

def main():
    """input_file = '/Users/perez/Tools/genome_annotations/hg38/annotation_bed_files/gene/hg38_annotation_gene_first_10_lines.bed'
    fasta_file = '/Users/perez/Tools/genomes/hg38/genome_files/aggregate_hg38/all_genome_files/hg38_all.fa'
    total_string_length = 23
    nonvariable_fixed_pam_length = 2
    nonvariable_fixed_forward_pam_sequence = 'GG'
    nonvariable_fixed_reverse_pam_sequence = 'CC'
    direction_5_or_3_prime = 3
    wildcard_position = 3
    outdir = '/Users/perez/Desktop'
    """

    # user input arguments
    input_file,fasta_file,outdir,total_string_length,nonvariable_fixed_pam_length,nonvariable_fixed_forward_pam_sequence,nonvariable_fixed_reverse_pam_sequence,direction_5_or_3_prime,wildcard_position,feature_dictionary = arg_parser()

    # processing
    ui_dict = csc_gRNA_lookup_processing(input_file,fasta_file,outdir,total_string_length,nonvariable_fixed_pam_length,nonvariable_fixed_forward_pam_sequence,nonvariable_fixed_reverse_pam_sequence,direction_5_or_3_prime,wildcard_position,target,feature_dictionary)

    # user end message
    sys.stdout.write('processing complete\n')

    #TODO allow for gene/feature name entry with lookup to coordinate

if __name__ == '__main__':
    main()
